package com.cimble.range;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cimble.dto.AbstractDTO;

/**
 * This class is responsible for holding date filter.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public class DateRange extends AbstractDTO {
	private static final long serialVersionUID = -6138102198188513597L;

	private String start;
	private String end;

	public DateRange() {
	}

	public DateRange(String start, String end) {
		this.start = start;
		this.end = end;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public boolean isValid() {
		return !(this.start.equals("-1") && this.end.equals("-1"));
	}

	public TimestampRange getTimestampRange() throws ParseException {
		TimestampRange range = new TimestampRange();
		Date stDate = new SimpleDateFormat("yyyy-MM-dd").parse(getStart());
		if (stDate != null) {
			range.setStart(stDate.getTime());
		}
		Date enDate = new SimpleDateFormat("yyyy-MM-dd").parse(getEnd());
		if (enDate != null) {
			range.setEnd(enDate.getTime() + (24 * 60 * 60 * 1000));
		}
		return range;
	}

}
