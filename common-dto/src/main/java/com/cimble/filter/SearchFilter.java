package com.cimble.filter;

import com.cimble.object.AbstractObject;

/**
 * This class is the base class for all filters.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public abstract class SearchFilter extends AbstractObject {
	private static final long serialVersionUID = 1234901290L;
    private String id;
    private String query;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

}
