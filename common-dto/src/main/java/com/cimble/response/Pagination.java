package com.cimble.response;

import com.cimble.paging.Paging;

/**
 * This class is intended for holding paging info.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public class Pagination extends Paging {
	private static final long serialVersionUID = 17897L;
	private int count;
	private int total;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
