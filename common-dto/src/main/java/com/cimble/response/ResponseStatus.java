package com.cimble.response;

/**
 * This class is intended for holding all possible status types.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public enum ResponseStatus {
	SUCCESS, ERROR, WARNING;
}
