package com.cimble.sort;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.dto.AbstractDTO;

/**
 * This class holds the sort rules.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public class SortRules extends AbstractDTO {
	private static final long serialVersionUID = -177004939860687447L;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SortRules.class.getName());

	private List<SortRule> rules = new LinkedList<SortRule>();
	
	public String ruleQuery;

	public SortRules() {
	}

	public void clear() {
		rules.clear();
	}

	public void add(SortRule sortRule) {
		rules.add(sortRule);
	}

	public void add(List<SortRule> rules) {
		this.rules.addAll(rules);
	}

	public List<SortRule> getRules() {
		return rules;
	}

	public void parse(String sort) {
		if (StringUtils.isBlank(sort)) {
			return;
		}
		String[] sorts = sort.split(",");
		for (String slot : sorts) {
			String[] vals = slot.split(" ");
			SortOrder sortOrder = SortOrder.ASC;
			try {
				sortOrder = SortOrder.valueOf(vals[1].toUpperCase());
			} catch (Exception e) {
				LOGGER.warn("Error parsing sort order for " + slot
						+ ". Cause : " + e.getMessage());
			}
			if (StringUtils.isNotBlank(vals[0])) {
				rules.add(new SortRule(vals[0], sortOrder));
			}
		}
	}

	public Map<String, Integer> getSortMap() {
		Map<String, Integer> sortMap = new LinkedHashMap<String, Integer>();
		for (SortRule sortRule : rules) {
			sortMap.put(sortRule.getFieldName(), sortRule.getValue().getCode());
		}
		return sortMap;
	}

	public String getRuleQuery() {
		return ruleQuery;
	}

	public void setRuleQuery(List<SortRule> rules) {
		StringBuilder query = new StringBuilder();
		for (SortRule sortRule : rules) {
			query.append(sortRule.getFieldName()+ " "+sortRule.getValue()+",");
		}
		query.reverse().replace(0, 1, "").reverse();
		
		this.ruleQuery = query.toString();
	}
	
	

}
