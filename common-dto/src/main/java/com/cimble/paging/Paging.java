package com.cimble.paging;

import com.cimble.dto.AbstractDTO;

/**
 * This class is intended for holding paging info.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public class Paging extends AbstractDTO {
	private static final long serialVersionUID = 7629575261222775892L;
	private int offset;
    private int limit = 10;

    public Paging() {
    }

    public Paging(int offset, int limit) {
        super();
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

}
