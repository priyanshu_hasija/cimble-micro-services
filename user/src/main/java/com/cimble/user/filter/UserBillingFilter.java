package com.cimble.user.filter;

import com.cimble.filter.SearchFilter;

/**
 * This class is responsible for holding generic fields to be returned in
 * response.
 * .
 * @version 1.0
 */
public class UserBillingFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private Long card_number;
	private String name_on_card;
	private Integer user_id;
	
	
	
	public Long getCard_number() {
		return card_number;
	}
	public void setCard_number(Long card_number) {
		this.card_number = card_number;
	}
	public String getName_on_card() {
		return name_on_card;
	}
	public void setName_on_card(String name_on_card) {
		this.name_on_card = name_on_card;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}	

	

	
	
	
}