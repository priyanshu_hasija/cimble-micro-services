package com.cimble.user.filter;

import com.cimble.filter.SearchFilter;


public class UserNotificationFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private Integer device_id;
	private Integer user_id;
	
	
	
	public Integer getDevice_id() {
		return device_id;
	}
	public void setDevice_id(Integer device_id) {
		this.device_id = device_id;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	
}