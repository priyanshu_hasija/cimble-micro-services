package com.cimble.user.filter;

import com.cimble.filter.SearchFilter;

/**
 * This class is responsible for holding generic fields to be returned in
 * response.
 * .
 * @version 1.0
 */
public class UserFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private Integer partner_id;
	private String user_name;

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}
	
	
	

	
	
	
}