package com.cimble.user.filter;

import com.cimble.filter.SearchFilter;

/**
 * This class is responsible for holding generic fields to be returned in
 * response.
 * 
 * @version 1.0
 */
public class DeviceFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private Integer user_id;
	private Integer device_id;
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public Integer getDevice_id() {
		return device_id;
	}
	public void setDevice_id(Integer device_id) {
		this.device_id = device_id;
	}
	
	
	
	
	
	
}
