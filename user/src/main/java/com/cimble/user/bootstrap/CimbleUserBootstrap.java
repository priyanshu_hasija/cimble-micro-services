package com.cimble.user.bootstrap;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.skife.jdbi.v2.DBI;

import com.cimble.bootstrap.AbstractBootstrap;
import com.cimble.user.config.CimbleUserBootstrapConfiguration;
import com.cimble.user.dao.CimbleDeviceDAO;
import com.cimble.user.dao.CimbleUserBillingDAO;
import com.cimble.user.dao.CimbleUserDAO;
import com.cimble.user.dao.CimbleUserLocationDAO;
import com.cimble.user.dao.CimbleUserNotificationDAO;
import com.cimble.user.management.CimbleDeviceManagement;
import com.cimble.user.management.CimbleUserBillingManagement;
import com.cimble.user.management.CimbleUserLocationManagement;
import com.cimble.user.management.CimbleUserManagement;
import com.cimble.user.management.CimbleUserNotificationManagement;
import com.cimble.user.management.DefaultCimbleDeviceManagement;
import com.cimble.user.management.DefaultCimbleUserBillingManagement;
import com.cimble.user.management.DefaultCimbleUserLocationManagement;
import com.cimble.user.management.DefaultCimbleUserManagement;
import com.cimble.user.management.DefaultCimbleUserNotificationManagement;
import com.cimble.user.service.impl.DefaultCimbleDeviceService;
import com.cimble.user.service.impl.DefaultCimbleUserBillingService;
import com.cimble.user.service.impl.DefaultCimbleUserLocationService;
import com.cimble.user.service.impl.DefaultCimbleUserNotificationService;
import com.cimble.user.service.impl.DefaultCimbleUserService;

/**
 * This class is intended for starting the application. It registers service, ui
 * & db migration modules.
 * 
 * @version 1.0
 */
public class CimbleUserBootstrap extends
		AbstractBootstrap<CimbleUserBootstrapConfiguration> {

	public static final String ID = "cimble";
	private CimbleUserManagement cimbleUserManagement;
	private CimbleUserBillingManagement cimbleUserBillingManagement;
	private CimbleDeviceManagement cimbleDeviceManagement;
	private CimbleUserLocationManagement cimbleUserLocationManagement;
	private CimbleUserNotificationManagement cimbleUserNotificationManagement;
	public static void main(String[] args) throws Exception {
		new CimbleUserBootstrap().run(args);
	}

	@Override
	public void initializeConfig(
			Bootstrap<CimbleUserBootstrapConfiguration> bootstrap) {
		bootstrap.addBundle(new MigrationsBundle<CimbleUserBootstrapConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(CimbleUserBootstrapConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
	}

	@Override
	public void runBootstrap(CimbleUserBootstrapConfiguration configuration,
			Environment environment) throws Exception {
		DBI dbi = new DBI(configuration.getDataSourceFactory().getUrl(),
				configuration.getDataSourceFactory().getUser(), configuration
						.getDataSourceFactory().getPassword());

		CimbleUserDAO ingestUserDao = dbi.onDemand(CimbleUserDAO.class);

		cimbleUserManagement = new DefaultCimbleUserManagement(ingestUserDao);
		

		environment.jersey().register(
				new DefaultCimbleUserService(cimbleUserManagement));

		
		CimbleUserBillingDAO ingestUserBillingDao = dbi.onDemand(CimbleUserBillingDAO.class);

		cimbleUserBillingManagement = new DefaultCimbleUserBillingManagement(ingestUserBillingDao);
		

		environment.jersey().register(
				new DefaultCimbleUserBillingService(cimbleUserBillingManagement));


		CimbleDeviceDAO ingestDeviceDao = dbi.onDemand(CimbleDeviceDAO.class);

		cimbleDeviceManagement = new DefaultCimbleDeviceManagement(ingestDeviceDao);
		

		environment.jersey().register(
				new DefaultCimbleDeviceService(cimbleDeviceManagement));
		
		CimbleUserLocationDAO ingestUserLocationDao = dbi.onDemand(CimbleUserLocationDAO.class);

		cimbleUserLocationManagement = new DefaultCimbleUserLocationManagement(ingestUserLocationDao);
		

		environment.jersey().register(
				new DefaultCimbleUserLocationService(cimbleUserLocationManagement));
		
		CimbleUserNotificationDAO ingestUserNotificationDao = dbi.onDemand(CimbleUserNotificationDAO.class);

		cimbleUserNotificationManagement = new DefaultCimbleUserNotificationManagement(ingestUserNotificationDao);
		

		environment.jersey().register(
				new DefaultCimbleUserNotificationService(cimbleUserNotificationManagement));
	}

	@Override
	public String getId() {
		return ID;
	}

}
