package com.cimble.user.management;

import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dto.UserNotification;
import com.cimble.user.filter.UserNotificationFilter;

public interface CimbleUserNotificationManagement {
	
	UserNotification save(UserNotification userNotification) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<UserNotification> list(UserNotificationFilter filter, Paging paging)
			throws CimbleException;
	
	int delete(UserNotificationFilter filter) throws CimbleException;
	
	UserNotification update(UserNotification userNotification, UserNotificationFilter filter) throws CimbleException;
	
	UserNotification findById(String id) throws CimbleException;

	UserNotification findOne(UserNotificationFilter filter);

}
