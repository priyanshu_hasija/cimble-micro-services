package com.cimble.user.management;

import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dao.CimbleUserLocationDAO;
import com.cimble.user.dto.UserBilling;
import com.cimble.user.dto.UserLocation;
import com.cimble.user.filter.UserLocationFilter;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public class DefaultCimbleUserLocationManagement implements CimbleUserLocationManagement{
	private CimbleUserLocationDAO cimbleUserLocationDAO;

	public DefaultCimbleUserLocationManagement(CimbleUserLocationDAO cimbleUserLocationDAO) {
		super();
		this.cimbleUserLocationDAO = cimbleUserLocationDAO;
	}

	@Override
	public List<UserLocation> list(UserLocationFilter filter, Paging paging)
			throws CimbleException {
		return cimbleUserLocationDAO.list(filter, paging);
	}

	@Override
	public UserLocation save(UserLocation userLocation) throws CimbleException {
		if(cimbleUserLocationDAO.save (userLocation) != 0)
		{
			return userLocation;
		}
		return new UserLocation();
	}
	
	@Override
	public int delete(UserLocationFilter filter) throws CimbleException {
		return cimbleUserLocationDAO.delete(filter);
	}
	
	@Override
	public UserLocation update(UserLocation userLocation,UserLocationFilter filter) throws CimbleException {
		if(cimbleUserLocationDAO.update(userLocation,filter) != 0)
		{
			return userLocation;
		}
		return new UserLocation();
	}
	
	@Override
	public UserLocation findById(String id) throws CimbleException {
		return cimbleUserLocationDAO.findById(id);
	}

	@Override
	public UserLocation findOne(UserLocationFilter filter) {
		return cimbleUserLocationDAO.findOne(filter);
	}


}
