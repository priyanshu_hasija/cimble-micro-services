package com.cimble.user.management;

import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dao.CimbleDeviceDAO;
import com.cimble.user.dto.Device;
import com.cimble.user.filter.DeviceFilter;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */
public class DefaultCimbleDeviceManagement implements CimbleDeviceManagement {
	private CimbleDeviceDAO cimbleDeviceDAO;

	public DefaultCimbleDeviceManagement(CimbleDeviceDAO cimbleDeviceDAO) {
		super();
		this.cimbleDeviceDAO = cimbleDeviceDAO;
	}

	@Override
	public List<Device> listDevice(DeviceFilter filter, Paging paging)
			throws CimbleException {
		return cimbleDeviceDAO.devicesByUserId(filter, paging);
	}

	@Override
	public Device saveDevice(Device device) throws CimbleException {
		if(cimbleDeviceDAO.save (device) != 0)
		{
			cimbleDeviceDAO.setState(device);
			return device;
		}
		return new Device();
	}
	
	@Override
	public int deleteDevice(DeviceFilter filter) throws CimbleException {
		return cimbleDeviceDAO.delete(filter);
	}
	
	@Override
	public Device updateDevice(Device device,DeviceFilter filter) throws CimbleException {
		
		if(cimbleDeviceDAO.updatedevice(device,filter) !=0)
		{
		
			
			return device;
		}
		return new Device();
	}
	
	@Override
	public Device findDeviceById(String id) throws CimbleException {
		return cimbleDeviceDAO.findById(id);
	}

	@Override
	public Device findDeviceOne(DeviceFilter filter) {
		return cimbleDeviceDAO.findOne(filter);
	}

}