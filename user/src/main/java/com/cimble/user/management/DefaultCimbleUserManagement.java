package com.cimble.user.management;

import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dao.CimbleUserDAO;
import com.cimble.user.dto.User;
import com.cimble.user.filter.UserFilter;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public class DefaultCimbleUserManagement implements CimbleUserManagement {
	private CimbleUserDAO cimbleUserDao;

	public DefaultCimbleUserManagement(CimbleUserDAO cimbleUserDao) {
		super();
		this.cimbleUserDao = cimbleUserDao;
	}

	@Override
	public List<User> listUser(UserFilter filter, Paging paging)
			throws CimbleException {
		return cimbleUserDao.list(filter, paging);
	}

	@Override
	public User saveUser(User user) throws CimbleException {
		if(cimbleUserDao.save (user) != 0)
		{
			return user;
		}
		return new User();
	}
	
	@Override
	public int deleteUser(UserFilter filter) throws CimbleException {
		return cimbleUserDao.delete(filter);
	}
	
	@Override
	public User updateUser(User user,UserFilter filter) throws CimbleException {
		if(cimbleUserDao.update(user,filter) != 0)
		{
			return user;
		}
		return new User();
	}
	
	@Override
	public User findUserById(String id) throws CimbleException {
		return cimbleUserDao.findById(id);
	}

	@Override
	public User findUserOne(UserFilter filter) {
		return cimbleUserDao.findOne(filter);
	}


}
