package com.cimble.user.management;

import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dao.CimbleUserBillingDAO;
import com.cimble.user.dao.CimbleUserDAO;
import com.cimble.user.dto.User;
import com.cimble.user.dto.UserBilling;
import com.cimble.user.filter.UserBillingFilter;
import com.cimble.user.filter.UserFilter;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public class DefaultCimbleUserBillingManagement implements CimbleUserBillingManagement{
	private CimbleUserBillingDAO cimbleUserBillingDAO;

	public DefaultCimbleUserBillingManagement(CimbleUserBillingDAO cimbleUserBillingDAO) {
		super();
		this.cimbleUserBillingDAO = cimbleUserBillingDAO;
	}

	@Override
	public List<UserBilling> list(UserBillingFilter filter, Paging paging)
			throws CimbleException {
		return cimbleUserBillingDAO.list(filter, paging);
	}

	@Override
	public UserBilling save(UserBilling userBilling) throws CimbleException {
		if(cimbleUserBillingDAO.save(userBilling) != 0)
		{
			return userBilling;
		}
		return new UserBilling();
	}
	
	@Override
	public int delete(UserBillingFilter filter) throws CimbleException {
		return cimbleUserBillingDAO.delete(filter);
	}
	
	@Override
	public UserBilling update(UserBilling userBilling,UserBillingFilter filter) throws CimbleException {
		if(cimbleUserBillingDAO.update(userBilling,filter) != 0)
		{
			return userBilling;
		}
		return new UserBilling();
	}
	
	@Override
	public UserBilling findById(String id) throws CimbleException {
		return cimbleUserBillingDAO.findById(id);
	}

	@Override
	public UserBilling findOne(UserBillingFilter filter) {
		return cimbleUserBillingDAO.findOne(filter);
	}


}
