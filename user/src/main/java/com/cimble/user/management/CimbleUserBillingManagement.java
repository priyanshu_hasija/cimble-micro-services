package com.cimble.user.management;
import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dto.UserBilling;
import com.cimble.user.filter.UserBillingFilter;

/**
 *This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public interface CimbleUserBillingManagement {
	
	/**
	 * 
	 * @param product Controls
	 * @return
	 * @throws CimbleException
	 */
	UserBilling save(UserBilling userBilling) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<UserBilling> list(UserBillingFilter filter, Paging paging)
			throws CimbleException;
	
	int delete(UserBillingFilter filter) throws CimbleException;
	
	UserBilling update(UserBilling userBilling, UserBillingFilter filter) throws CimbleException;
	
	UserBilling findById(String id) throws CimbleException;

	UserBilling findOne(UserBillingFilter filter);

}
