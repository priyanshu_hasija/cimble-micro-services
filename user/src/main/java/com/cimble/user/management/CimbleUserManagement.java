package com.cimble.user.management;
import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dto.User;
import com.cimble.user.filter.UserFilter;

/**
 *This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public interface CimbleUserManagement {
	
	/**
	 * 
	 * @param product Controls
	 * @return
	 * @throws CimbleException
	 */
	User saveUser(User user) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<User> listUser(UserFilter filter, Paging paging)
			throws CimbleException;
	
	int deleteUser(UserFilter filter) throws CimbleException;
	
	User updateUser(User productControls, UserFilter filter) throws CimbleException;
	
	User findUserById(String id) throws CimbleException;

	User findUserOne(UserFilter filter);

}
