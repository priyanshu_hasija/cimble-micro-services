package com.cimble.user.management;

import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dao.CimbleUserNotificationDAO;
import com.cimble.user.dto.UserNotification;
import com.cimble.user.filter.UserNotificationFilter;


public class DefaultCimbleUserNotificationManagement implements CimbleUserNotificationManagement{
	private CimbleUserNotificationDAO cimbleUserNotificationDAO;

	public DefaultCimbleUserNotificationManagement(CimbleUserNotificationDAO cimbleUserNotificationDAO) {
		super();
		this.cimbleUserNotificationDAO = cimbleUserNotificationDAO;
	}

	@Override
	public List<UserNotification> list(UserNotificationFilter filter, Paging paging)
			throws CimbleException {
		return cimbleUserNotificationDAO.list(filter, paging);
	}

	@Override
	public UserNotification save(UserNotification userNotification) throws CimbleException {
		if(cimbleUserNotificationDAO.save (userNotification) != 0)
		{
			return userNotification;
		}
		return new UserNotification();
	}
	
	@Override
	public int delete(UserNotificationFilter filter) throws CimbleException {
		return cimbleUserNotificationDAO.delete(filter);
	}
	
	@Override
	public UserNotification update(UserNotification userNotification,UserNotificationFilter filter) throws CimbleException {
		if(cimbleUserNotificationDAO.update(userNotification,filter) != 0)
		{
			return userNotification;
		}
		return new UserNotification();
	}
	
	@Override
	public UserNotification findById(String id) throws CimbleException {
		return cimbleUserNotificationDAO.findById(id);
	}

	@Override
	public UserNotification findOne(UserNotificationFilter filter) {
		return cimbleUserNotificationDAO.findOne(filter);
	}

}

