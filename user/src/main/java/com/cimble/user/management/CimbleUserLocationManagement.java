package com.cimble.user.management;
import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dto.UserLocation;
import com.cimble.user.filter.UserLocationFilter;

/**
 *This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public interface CimbleUserLocationManagement {
	
	/**
	 * 
	 * @param product Controls
	 * @return
	 * @throws CimbleException
	 */
	UserLocation save(UserLocation userLocation) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<UserLocation> list(UserLocationFilter filter, Paging paging)
			throws CimbleException;
	
	int delete(UserLocationFilter filter) throws CimbleException;
	
	UserLocation update(UserLocation userLocation, UserLocationFilter filter) throws CimbleException;
	
	UserLocation findById(String id) throws CimbleException;

	UserLocation findOne(UserLocationFilter filter);

}
