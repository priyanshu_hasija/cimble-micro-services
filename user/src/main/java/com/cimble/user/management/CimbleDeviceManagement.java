package com.cimble.user.management;

import java.util.List;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.user.dto.Device;
import com.cimble.user.filter.DeviceFilter;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public interface CimbleDeviceManagement {

	/**
	 * 
	 * @param partner
	 * @return
	 * @throws CimbleException
	 */
	Device saveDevice(Device device) throws CimbleException;
	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	
	List<Device> listDevice(DeviceFilter filter, Paging paging)
			throws CimbleException;

	int deleteDevice(DeviceFilter filter) throws CimbleException;
	
	Device updateDevice(Device device, DeviceFilter filter) throws CimbleException;
	
	Device findDeviceById(String id) throws CimbleException;

	Device findDeviceOne(DeviceFilter filter);

}
