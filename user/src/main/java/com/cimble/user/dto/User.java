package com.cimble.user.dto;

import com.cimble.dto.AbstractAuditDTO;
/**
 * This interface is intended for providing interactions with product_controls table.
 * 
 * @version 1.0
 */

public class User  extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L; 
	
			
			
	private String first_name;
	private String last_name;
	private String user_name;
	private String email_address;
	private Integer partner_id;
	private String language_code;
	private String country_code;
	
	
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getEmail_address() {
		return email_address;
	}
	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}
	public Integer getPartner_id() {
		return partner_id;
	}
	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}
	public String getLanguage_code() {
		return language_code;
	}
	public void setLanguage_code(String language_code) {
		this.language_code = language_code;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	
	
	
		
}
	