package com.cimble.user.dto;

import com.cimble.dto.AbstractAuditDTO;
/**
 * This interface is intended for providing interactions with user_billing table.
 * 
 * @version 1.0
 */

public class UserNotification  extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L; 
	
	private Integer user_id;	
	private Integer device_id;
	private String notification_desc;
	
	
	
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public Integer getDevice_id() {
		return device_id;
	}
	public void setDevice_id(Integer device_id) {
		this.device_id = device_id;
	}
	public String getNotification_desc() {
		return notification_desc;
	}
	public void setNotification_desc(String notification_desc) {
		this.notification_desc = notification_desc;
	}
	
	
			
}
	
