package com.cimble.user.dto;

import com.cimble.dto.AbstractAuditDTO;
/**
 * This interface is intended for providing interactions with user_billing table.
 * 
 * @version 1.0
 */

public class UserLocation  extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L; 
	
	
	private Integer user_id;	
	private Integer device_id;
	private String location_name;
	private String location_address;
	private String location_status;
	


	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public Integer getDevice_id() {
		return device_id;
	}
	public void setDevice_id(Integer device_id) {
		this.device_id = device_id;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getLocation_address() {
		return location_address;
	}
	public void setLocation_address(String location_address) {
		this.location_address = location_address;
	}
	public String getLocation_status() {
		return location_status;
	}
	public void setLocation_status(String location_status) {
		this.location_status = location_status;
	}
	
	
	
	
}
	