package com.cimble.user.dto;

import java.sql.Date;

import com.cimble.dto.AbstractAuditDTO;

/**
 * This class is intended for holding ingest information.
 * 
 * @version 1.0
 */
public class Device extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L;

	
	private Date billeable_date;
	private Integer billing_code; 
	private Integer bill_to_user_id;
	private Integer daylight;
	private Integer mask_id;
	private Integer device_group_id;
	private Integer object_id;
	private Integer driver_user_id;
	private Integer product_id;
	private Integer icon_id;
	private String description;
	private String device_type;
	private String esn_id;
	private String page_min;
	private String password;
	private String state;
	private Date installed_date;
	private String time_zone;
	
	private Integer user_id;
	private Integer device_id;
	private Long time_stamp;
	
	
	
	public Date getBilleable_date() {
		return billeable_date;
	}
	public void setBilleable_date(Date billeable_date) {
		this.billeable_date = billeable_date;
	}
	public Integer getBilling_code() {
		return billing_code;
	}
	public void setBilling_code(Integer billing_code) {
		this.billing_code = billing_code;
	}
	public Integer getBill_to_user_id() {
		return bill_to_user_id;
	}
	public void setBill_to_user_id(Integer bill_to_user_id) {
		this.bill_to_user_id = bill_to_user_id;
	}
	public Integer getDaylight() {
		return daylight;
	}
	public void setDaylight(Integer daylight) {
		this.daylight = daylight;
	}
	public Integer getMask_id() {
		return mask_id;
	}
	public void setMask_id(Integer mask_id) {
		this.mask_id = mask_id;
	}
	public Integer getDevice_group_id() {
		return device_group_id;
	}
	public void setDevice_group_id(Integer device_group_id) {
		this.device_group_id = device_group_id;
	}
	public Integer getObject_id() {
		return object_id;
	}
	public void setObject_id(Integer object_id) {
		this.object_id = object_id;
	}
	public Integer getDriver_user_id() {
		return driver_user_id;
	}
	public void setDriver_user_id(Integer driver_user_id) {
		this.driver_user_id = driver_user_id;
	}
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public Integer getIcon_id() {
		return icon_id;
	}
	public void setIcon_id(Integer icon_id) {
		this.icon_id = icon_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDevice_type() {
		return device_type;
	}
	public void setDevice_type(String device_type) {
		this.device_type = device_type;
	}
	public String getEsn_id() {
		return esn_id;
	}
	public void setEsn_id(String esn_id) {
		this.esn_id = esn_id;
	}
	public String getPage_min() {
		return page_min;
	}
	public void setPage_min(String page_min) {
		this.page_min = page_min;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getInstalled_date() {
		return installed_date;
	}
	public void setInstalled_date(Date installed_date) {
		this.installed_date = installed_date;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public Integer getDevice_id() {
		return device_id;
	}
	public void setDevice_id(Integer device_id) {
		this.device_id = device_id;
	}
	public Long getTime_stamp() {
		return time_stamp;
	}
	public void setTime_stamp(Long time_stamp) {
		this.time_stamp = time_stamp;
	}
	
	
}
