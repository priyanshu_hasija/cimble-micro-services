package com.cimble.user.dto;

import com.cimble.dto.AbstractAuditDTO;
/**
 * This interface is intended for providing interactions with user_billing table.
 * 
 * @version 1.0
 */

public class UserBilling  extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L; 
	
	
	private Integer user_id;	
	private String card_type;
	private Long card_number;
	private Integer month;
	private Integer year;
	private String name_on_card;
	private String addr1;
	private String addr2;
	private String city;
	private String state;
	
	
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getCard_type() {
		return card_type;
	}
	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}
	public Long getCard_number() {
		return card_number;
	}
	public void setCard_number(Long card_number) {
		this.card_number = card_number;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getName_on_card() {
		return name_on_card;
	}
	public void setName_on_card(String name_on_card) {
		this.name_on_card = name_on_card;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Integer getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(Integer postal_code) {
		this.postal_code = postal_code;
	}
	public Integer getCountry_code() {
		return country_code;
	}
	public void setCountry_code(Integer country_code) {
		this.country_code = country_code;
	}
	private Integer postal_code;
	private Integer country_code;
	
	
}
	