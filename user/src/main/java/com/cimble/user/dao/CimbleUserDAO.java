package com.cimble.user.dao;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.user.dao.mapper.UserMapper;
import com.cimble.user.dto.User;
import com.cimble.user.filter.UserFilter;
import com.cimble.dao.AbstractRelationalDao;

/**
 * This interface is intended for providing interactions with Product_controls table.
 * 
 * @version 1.0
 */

	@RegisterMapper(UserMapper.class)
	@UseStringTemplate3StatementLocator
	public abstract class CimbleUserDAO extends
			AbstractRelationalDao<User, UserFilter, UserMapper> {

		public CimbleUserDAO() throws Exception {
			super(UserMapper.class);
		}
	}
