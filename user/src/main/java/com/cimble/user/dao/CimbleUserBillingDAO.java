package com.cimble.user.dao;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.dao.AbstractRelationalDao;
import com.cimble.user.dao.mapper.UserBillingMapper;
import com.cimble.user.dto.UserBilling;
import com.cimble.user.filter.UserBillingFilter;

/**
 * This interface is intended for providing interactions with User Billing table.
 * 
 * @version 1.0
 */

	@RegisterMapper(UserBillingMapper.class)
	@UseStringTemplate3StatementLocator
	public abstract class CimbleUserBillingDAO extends
			AbstractRelationalDao<UserBilling, UserBillingFilter, UserBillingMapper> {

		public CimbleUserBillingDAO() throws Exception {
			super(UserBillingMapper.class);
		}
	}
