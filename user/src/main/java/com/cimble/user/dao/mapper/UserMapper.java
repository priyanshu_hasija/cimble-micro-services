package com.cimble.user.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.mapper.RequestMapper;
import com.cimble.user.dto.User;
import com.cimble.user.filter.UserFilter;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Partner}
 * .
 * 
 * @version 1.0
 */
public class UserMapper implements ResultSetMapper<User>,
		RequestMapper<User, UserFilter> {

	private static final String TABLE_NAME = "SUBSCRIBERS";
	
	
	
	
	
	private static final String ID = "ID";
	private static final String FIRST_NAME = "FIRST_NAME";
	private static final String LAST_NAME ="LAST_NAME";
	private static final String USER_NAME ="USER_NAME";
	private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
	private static final String LANGUAGE_CODE = "LANGUAGE_CODE";
	private static final String COUNTRY_CODE ="COUNTRY_CODE";

	
	@Override
	public User map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		User obj = new User();
		obj.setId(rs.getString(ID));
		obj.setFirst_name(rs.getString(FIRST_NAME));
		obj.setUser_name(rs.getString(USER_NAME));
		obj.setLast_name(rs.getString(LAST_NAME));
		obj.setEmail_address(rs.getString(EMAIL_ADDRESS));
		obj.setLanguage_code(rs.getString(LANGUAGE_CODE));
		obj.setCountry_code(rs.getString(COUNTRY_CODE));
		return obj;
	}

	@Override
	public Map<String, Object> convertToDao(User dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (StringUtils.isNotBlank(dto.getId()))
			map.put(ID, dto.getId());		
		if (StringUtils.isNotBlank(dto.getFirst_name()))
			map.put(FIRST_NAME, dto.getFirst_name());
		if (StringUtils.isNotBlank(dto.getLast_name()))
			map.put(LAST_NAME, dto.getLast_name());
		if (StringUtils.isNotBlank(dto.getUser_name()))
			map.put(USER_NAME, dto.getUser_name());
		if (StringUtils.isNotBlank(dto.getEmail_address()))
			map.put(EMAIL_ADDRESS, dto.getEmail_address());
		if (StringUtils.isNotBlank(dto.getLanguage_code()))
			map.put(LANGUAGE_CODE, dto.getLanguage_code());
		if (StringUtils.isNotBlank(dto.getCountry_code()))
			map.put(COUNTRY_CODE, dto.getCountry_code());
		if (dto.getCreatedOn() != null)
			map.put(User.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(User.MODIFIED_ON, dto.getModifiedOn());
		
		return map;
	}

	@Override
	public Map<String, Object> convertToDao(UserFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			if(StringUtils.isNotBlank(filter.getId()))
				map.put(ID, filter.getId());
			if (StringUtils.isNotBlank(filter.getUser_name()))
				map.put(USER_NAME, filter.getUser_name());
			

			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}