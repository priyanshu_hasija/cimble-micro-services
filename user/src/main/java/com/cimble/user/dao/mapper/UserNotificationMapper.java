package com.cimble.user.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.mapper.RequestMapper;
import com.cimble.user.dto.UserNotification;
import com.cimble.user.filter.UserNotificationFilter;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link USER BILLING}
 * .
 * 
 * @version 1.0
 */
public class UserNotificationMapper implements ResultSetMapper<UserNotification>,
		RequestMapper<UserNotification, UserNotificationFilter> {

	private static final String TABLE_NAME = "USER_NOTIFICATIONS";
	
	private static final String USER_ID = "USER_ID";
	private static final String ESN_ID ="ESN_ID";
	private static final String NOTIFICATION_DESC = "NOTIFICATION_DESC";
	private static final String ID = "ID";
	
	@Override
	public UserNotification map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		UserNotification obj = new UserNotification();

		obj.setUser_id(rs.getInt(USER_ID));
		obj.setDevice_id(rs.getInt(ESN_ID));
		obj.setNotification_desc(rs.getString(NOTIFICATION_DESC));
		obj.setId(rs.getString(ID));
		return obj;
	}

	@Override
	public Map<String, Object> convertToDao(UserNotification dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (dto.getUser_id() != null)
			map.put(USER_ID, dto.getUser_id());
		if (dto.getDevice_id() != null)
			map.put(ESN_ID, dto.getDevice_id());
		if (StringUtils.isNotBlank(dto.getId() ))
			map.put(ID, dto.getId());
		if (StringUtils.isNotBlank(dto.getNotification_desc()))
			map.put(NOTIFICATION_DESC, dto.getNotification_desc());
		if (dto.getCreatedOn() != null)
			map.put(UserNotification.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(UserNotification.MODIFIED_ON, dto.getModifiedOn());
		
		
		return map;
	}

	@Override
	public Map<String, Object> convertToDao(UserNotificationFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			if(filter.getUser_id() != null)
				map.put(USER_ID, filter.getUser_id());
			if(filter.getDevice_id() != null)
				map.put(ESN_ID, filter.getDevice_id());
			if(StringUtils.isNotBlank(filter.getId()))
				map.put(ID, filter.getId());
			
			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}