package com.cimble.user.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.mapper.RequestMapper;
import com.cimble.user.dto.UserLocation;
import com.cimble.user.filter.UserLocationFilter;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link USER BILLING}
 * .
 * 
 * @version 1.0
 */
public class UserLocationMapper implements ResultSetMapper<UserLocation>,
		RequestMapper<UserLocation, UserLocationFilter> {

	private static final String TABLE_NAME = "USER_LOCATION";
	
	private static final String USER_ID = "USER_ID";
	private static final String ESN_ID ="ESN_ID";
	private static final String ID = "ID";
	private static final String LOCATION_NAME = "LOCATION_NAME";
	private static final String LOCATION_ADD= "LOCATION_ADD";
	private static final String LOC_STATUS ="LOC_STATUS";
	
	
	
	@Override
	public UserLocation map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		UserLocation obj = new UserLocation();

		obj.setUser_id(rs.getInt(USER_ID));
		obj.setDevice_id(rs.getInt(ESN_ID));
		obj.setId(rs.getString(ID));
		obj.setLocation_name(rs.getString(LOCATION_NAME));
		obj.setLocation_address(rs.getString(LOCATION_ADD));
		obj.setLocation_status(rs.getString(LOC_STATUS));

		return obj;
	}

	@Override
	public Map<String, Object> convertToDao(UserLocation dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (dto.getUser_id() != null)
			map.put(USER_ID, dto.getUser_id());
		if (dto.getDevice_id() != null)
			map.put(ESN_ID, dto.getDevice_id());
		if (StringUtils.isNotBlank(dto.getId() ))
			map.put(ID, dto.getId());
		if (StringUtils.isNotBlank(dto.getLocation_name()))
			map.put(LOCATION_NAME, dto.getLocation_name());
		if (StringUtils.isNotBlank(dto.getLocation_address()))
			map.put(LOCATION_ADD, dto.getLocation_address());
		if (StringUtils.isNotBlank(dto.getLocation_status()))
			map.put(LOC_STATUS, dto.getLocation_status());
		if (dto.getCreatedOn() != null)
			map.put(UserLocation.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(UserLocation.MODIFIED_ON, dto.getModifiedOn());
						
		return map;
	}

	@Override
	public Map<String, Object> convertToDao(UserLocationFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			if(StringUtils.isNotBlank(filter.getId()))
				map.put(ID, filter.getId());
			if(filter.getDevice_id() != null)
				map.put(ESN_ID, filter.getDevice_id());
			if(filter.getUser_id() != null)
				map.put(USER_ID, filter.getUser_id());
			
			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}