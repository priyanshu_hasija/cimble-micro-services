package com.cimble.user.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.constants.Punctuation;
import com.cimble.dao.AbstractRelationalDao;
import com.cimble.paging.Paging;
import com.cimble.user.dao.mapper.DeviceMapper;
import com.cimble.user.dto.Device;
import com.cimble.user.filter.DeviceFilter;

/**
 * This interface is intended for providing interactions with ingest table.
 * 
 * @version 1.0
 */
@RegisterMapper(DeviceMapper.class)
@UseStringTemplate3StatementLocator
public abstract class CimbleDeviceDAO extends
		AbstractRelationalDao<Device, DeviceFilter, DeviceMapper> {

	public CimbleDeviceDAO() throws Exception {
		super(DeviceMapper.class);
	}
	
	public List<Device> devicesByUserId(DeviceFilter filter,Paging paging) 
	{
		StringJoiner queryParams;
		List<Device> devices = new ArrayList<Device>();
		try {
			queryParams = join(AND, DeviceMapper.class.newInstance().convertToDao(filter));
			devices = list("DEVICES","where ID in (select device_id from SUBSCRIBER_DEVICES where " + (queryParams.length() == 0 ? Punctuation.SPACE.toString(): queryParams.toString()) +")",paging );
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return devices;
	}
	
	public void setState(Device device){
		
		 update("DEVICES", "state='registered'","where ID = "+ device.getDevice_id()+""); 
				
	}
	
	public int updatedevice(Device device, DeviceFilter filter){
		return update("DEVICES", "DESCRIPTION='"+device.getDescription()+"',ICON_ID="+device.getIcon_id(),"where ID = "+ filter.getDevice_id());
	}
	
	
	
	
	/*  StringJoiner setFields = join(Punctuation.COMMA.toString(), instance.convertToDao(dto));
      if (setFields.length() == 0) {
          return 0;
      }
      StringJoiner queryParams = join(AND, instance.convertToDao(filter));
      return update(instance.getTableName(), setFields.toString(),
              queryParams.length() == 0 ? Punctuation.SPACE.toString() : (WHERE + queryParams.toString()));
  */
	
	
	
}

