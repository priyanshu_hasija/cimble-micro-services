package com.cimble.user.dao;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.dao.AbstractRelationalDao;
import com.cimble.user.dao.mapper.UserNotificationMapper;
import com.cimble.user.dto.UserNotification;
import com.cimble.user.filter.UserNotificationFilter;

/**
 * This interface is intended for providing interactions with User Location table.
 * 
 * @version 1.0
 */

	@RegisterMapper(UserNotificationMapper.class)
	@UseStringTemplate3StatementLocator
	public abstract class CimbleUserNotificationDAO extends
			AbstractRelationalDao<UserNotification, UserNotificationFilter, UserNotificationMapper> {

		public CimbleUserNotificationDAO() throws Exception {
			super(UserNotificationMapper.class);
		}
	}
	
