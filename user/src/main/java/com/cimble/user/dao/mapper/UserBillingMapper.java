package com.cimble.user.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.mapper.RequestMapper;
import com.cimble.user.dto.UserBilling;
import com.cimble.user.filter.UserBillingFilter;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link USER BILLING}
 * .
 * 
 * @version 1.0
 */
public class UserBillingMapper implements ResultSetMapper<UserBilling>,
		RequestMapper<UserBilling, UserBillingFilter> {

	private static final String TABLE_NAME = "BILLING_DETAILS";
	private static final String USER_ID= "USER_ID";
	private static final String ID= "ID";
	private static final String CARD_TYPE = "CARD_TYPE";
	private static final String CARD_NUMBER ="CARD_NUMBER";
	private static final String MONTH = "MONTH";
	private static final String YEAR = "YEAR";
	private static final String NAME_ON_CARD = "NAME_ON_CARD";
	private static final String ADDR1 ="ADDR1";
	private static final String ADDR2 ="ADDR2";
	private static final String CITY ="CITY";
	private static final String STATE ="STATE";
	private static final String POSTAL_CODE = "POSTAL_CODE";
	private static final String COUNTRY_CODE ="COUNTRY_CODE";
	@Override
	public UserBilling map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		UserBilling obj = new UserBilling();

		obj.setCard_type(rs.getString(CARD_TYPE));
		obj.setCard_number(rs.getLong(CARD_NUMBER));
		obj.setMonth(rs.getInt(MONTH));
		obj.setId(rs.getString(ID));
		obj.setYear(rs.getInt(YEAR));
		obj.setUser_id(rs.getInt(USER_ID));
		obj.setName_on_card(rs.getString(NAME_ON_CARD));
		obj.setAddr1(rs.getString(ADDR1));
		obj.setAddr2(rs.getString(ADDR2));
		obj.setCity(rs.getString(CITY));
		obj.setState(rs.getString(STATE));
		obj.setPostal_code(rs.getInt(POSTAL_CODE));
		obj.setCountry_code(rs.getInt(COUNTRY_CODE));
		return obj;
	}

	@Override
	public Map<String, Object> convertToDao(UserBilling dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (StringUtils.isNotBlank(dto.getId()))
			map.put(ID, dto.getId());
		if (StringUtils.isNotBlank(dto.getCard_type()))
			map.put(CARD_TYPE, dto.getCard_type());
		if (dto.getCard_number() != null)
			map.put(CARD_NUMBER, dto.getCard_number());
		if (dto.getMonth() != null)
			map.put(MONTH, dto.getMonth());
		if (dto.getYear() != null)
			map.put(YEAR, dto.getYear());
		if (dto.getUser_id() != null)
			map.put(USER_ID, dto.getUser_id());
		if (StringUtils.isNotBlank(dto.getName_on_card()))
			map.put(NAME_ON_CARD, dto.getName_on_card());
		if (StringUtils.isNotBlank(dto.getAddr1()))
			map.put(ADDR1, dto.getAddr1());
		if (StringUtils.isNotBlank(dto.getAddr2()))
			map.put(ADDR2, dto.getAddr2());
		if (StringUtils.isNotBlank(dto.getCity()))
			map.put(CITY, dto.getCity());
		if (StringUtils.isNotBlank(dto.getState()))
			map.put(STATE, dto.getState());
		if (dto.getPostal_code()!= null)
			map.put(POSTAL_CODE, dto.getPostal_code());
		
		if (dto.getCountry_code()!= null)
			map.put(COUNTRY_CODE, dto.getCountry_code());
		if (dto.getCreatedOn() != null)
			map.put(UserBilling.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(UserBilling.MODIFIED_ON, dto.getModifiedOn());
		
		return map;
	}

	@Override
	public Map<String, Object> convertToDao(UserBillingFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			
			if(filter.getCard_number() != null)
				map.put(CARD_NUMBER, filter.getCard_number());
			if(filter.getUser_id() != null)
				map.put(USER_ID, filter.getUser_id());
			if(StringUtils.isNotBlank(filter.getName_on_card()))
				map.put(NAME_ON_CARD, filter.getName_on_card());
			if(StringUtils.isNotBlank(filter.getId()))
				map.put(ID, filter.getId());

			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}