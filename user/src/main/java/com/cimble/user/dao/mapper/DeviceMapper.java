package com.cimble.user.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.mapper.RequestMapper;
import com.cimble.user.dto.Device;
import com.cimble.user.filter.DeviceFilter;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Device}
 * 
 * @version 1.0
 */
public class DeviceMapper implements ResultSetMapper<Device>,
		RequestMapper<Device, DeviceFilter> {

	private static final String TABLE_NAME = "USER_DEVICES";
	private static final String subscriber_id = "subscriber_id";
	private static final String DEVICE_ID="DEVICE_ID";
	private static final String TIME_STAMP="TIME_STAMP";
	
	
	
	private static final String BILLEABLE_DATE = "BILLEABLE_DATE";
	private static final String BILLING_CODE = "BILLING_CODE";
	private static final String BILL_TO_USER_ID = "BILL_TO_USER_ID";
	private static final String DAYLIGHT = "DAYLIGHT";
	private static final String DESCRIPTION ="DESCRIPTION";
	private static final String DEVICE_GROUP_ID = "DEVICE_GROUP_ID";
	private static final String DEVICE_TYPE = "DEVICE_TYPE";
	private static final String DRIVER_USER_ID = "DRIVER_USER_ID";
	private static final String ID= "ID";
	private static final String ICON_ID="ICON_ID";
	private static final String INSTALLED_DATE ="INSTALLED_DATE";
	private static final String MASK_ID ="MASK_ID";
	private static final String OBJECT_ID ="OBJECT_ID";
	private static final String PAGE_MIN = "PAGE_MIN";
	private static final String PASSWORD ="PASSWORD";
	private static final String PRODUCT_ID ="PRODUCT_ID";
	private static final String STATE ="STATE";
	private static final  String TIMEZONE ="TIMEZONE";
	@Override
	public Device map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		Device obj = new Device();
		obj.setUser_id(rs.getInt(subscriber_id));
		obj.setDevice_id(rs.getInt(DEVICE_ID));
		obj.setTime_stamp(rs.getLong(TIME_STAMP));
		obj.setBilleable_date(rs.getDate(BILLEABLE_DATE));
		obj.setBilling_code(rs.getInt(BILLING_CODE));
		obj.setBill_to_user_id(rs.getInt(BILL_TO_USER_ID));
		obj.setDaylight(rs.getInt(DAYLIGHT));
		obj.setMask_id(rs.getInt(MASK_ID));
		obj.setDevice_group_id(rs.getInt(DEVICE_GROUP_ID));
		obj.setObject_id(rs.getInt(OBJECT_ID));
		obj.setDriver_user_id(rs.getInt(DRIVER_USER_ID));
		obj.setProduct_id(rs.getInt(PRODUCT_ID));
		obj.setIcon_id(rs.getInt(ICON_ID));
		obj.setDescription(rs.getString(DESCRIPTION));
		obj.setDevice_type(rs.getString(DEVICE_TYPE));
		obj.setEsn_id(rs.getString(ID));
		obj.setPage_min(rs.getString(PAGE_MIN));
		obj.setPassword(rs.getString(PASSWORD));
		obj.setState(rs.getString(STATE));
		obj.setInstalled_date(rs.getDate(INSTALLED_DATE));
		obj.setTime_zone(rs.getString(TIMEZONE));
		return obj;
	}

	@Override
	public Map<String, Object> convertToDao(Device dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (dto.getUser_id() != null)
			map.put(subscriber_id, dto.getUser_id());
		if (dto.getDevice_id() != null)
			map.put(DEVICE_ID, dto.getDevice_id());
		if (dto.getTime_stamp() != null)
			map.put(TIME_STAMP, dto.getTime_stamp());
		
		
		if (dto.getBilleable_date() != null)
			map.put(BILLEABLE_DATE, dto.getBilleable_date());
		
		if (dto.getBilling_code() != null)
			map.put(BILLING_CODE, dto.getBilling_code());
		if (dto.getBill_to_user_id() != null)
			map.put(BILL_TO_USER_ID, dto.getBill_to_user_id());
		if (dto.getDaylight() != null)
			map.put(DAYLIGHT, dto.getDaylight());
		if (dto.getMask_id() != null)
			map.put(MASK_ID, dto.getMask_id());
		if (dto.getDevice_group_id() != null)
			map.put(DEVICE_GROUP_ID, dto.getDevice_group_id());
		if (dto.getObject_id() != null)
			map.put(OBJECT_ID, dto.getObject_id());
		if (dto.getDriver_user_id() != null)
			map.put(DRIVER_USER_ID, dto.getDriver_user_id());
		if (dto.getProduct_id() != null)
			map.put(PRODUCT_ID, dto.getProduct_id());
		if (dto.getIcon_id() != null)
			map.put(ICON_ID, dto.getIcon_id());
		if(StringUtils.isNotBlank(dto.getDescription()))
			map.put(DESCRIPTION, dto.getDescription());
		
		if(StringUtils.isNotBlank(dto.getDevice_type()))
			map.put(DEVICE_TYPE, dto.getDevice_type());
		if(StringUtils.isNotBlank(dto.getEsn_id()))
			map.put(ID, dto.getEsn_id());
		if(StringUtils.isNotBlank(dto.getPage_min()))
			map.put(PAGE_MIN, dto.getPage_min());
		if(StringUtils.isNotBlank(dto.getPassword()))
			map.put(PASSWORD, dto.getPassword());
		if(StringUtils.isNotBlank(dto.getState()))
			map.put(STATE, dto.getState());
		if(StringUtils.isNotBlank(dto.getTime_zone()))
			map.put(TIMEZONE, dto.getTime_zone());
		
		if (dto.getInstalled_date() != null)
			map.put(INSTALLED_DATE, dto.getInstalled_date());
		if (dto.getCreatedOn() != null)
			map.put(Device.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(Device.MODIFIED_ON, dto.getModifiedOn());
		
		return map;
	}

	@Override
	public Map<String, Object> convertToDao(DeviceFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			if(filter.getUser_id() != null)
				map.put(subscriber_id, filter.getUser_id());
			if(filter.getDevice_id() != null)
				map.put(DEVICE_ID, filter.getDevice_id());

			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}