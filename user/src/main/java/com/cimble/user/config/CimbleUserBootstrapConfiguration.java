package com.cimble.user.config;

import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.cimble.config.AbstractBootstrapConfiguration;

/**
 * This class is intended for holding all the configurations injected from the external *.yml file.
 * 
 * @version 1.0
 */
public class CimbleUserBootstrapConfiguration extends AbstractBootstrapConfiguration {

    @Valid
    @NotNull
    @JsonProperty
    private final DataSourceFactory dataSourceFactory = new DataSourceFactory();


    public DataSourceFactory getDataSourceFactory() {
        return dataSourceFactory;
    }


}
