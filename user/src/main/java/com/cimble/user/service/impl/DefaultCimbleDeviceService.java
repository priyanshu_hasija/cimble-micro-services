package com.cimble.user.service.impl;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.paging.Paging;
import com.cimble.service.AbstractUserService;
import com.cimble.user.dto.Device;
import com.cimble.user.filter.DeviceFilter;
import com.cimble.user.management.CimbleDeviceManagement;
import com.cimble.user.service.CimbleDeviceService;

/**
 * This interface is intended for providing services related to cimble user billling.
 * 
 * @version 1.0
 */
@Path("/api/v1/user/{id}/device")
@Produces(MediaType.APPLICATION_JSON)

public class DefaultCimbleDeviceService extends AbstractUserService<Device, DeviceFilter>
implements CimbleDeviceService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleDeviceService.class);
	
	private CimbleDeviceManagement cimbleDeviceManagement;

	public DefaultCimbleDeviceService(CimbleDeviceManagement cimbleDeviceManagement) {
		super(Device.class, DeviceFilter.class);
		this.cimbleDeviceManagement = cimbleDeviceManagement;
	}

	@Override
	protected Response update(Device dto, DeviceFilter filter,
			String id) throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleDeviceManagement.updateDevice(dto, filter));
	}
	
	@Override
	protected Response list(DeviceFilter filter, Paging paging, String id)
			throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleDeviceManagement.listDevice(filter, paging) );
	}
	
	@Override
	protected Response findById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response deleteById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response save(Device dto, String id) throws Exception {
		dto.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleDeviceManagement.saveDevice(dto));
	}

	@Override
	protected Response delete(DeviceFilter filter) throws Exception {
		// TODO Auto-generated method stub
		/*filter.setUserId(Integer.parseInt(id));*/
		return onSuccess(cimbleDeviceManagement.deleteDevice(filter));
	
	}

	

}