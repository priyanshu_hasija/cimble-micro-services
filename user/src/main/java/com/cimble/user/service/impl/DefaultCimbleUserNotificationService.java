package com.cimble.user.service.impl;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.paging.Paging;
import com.cimble.service.AbstractUserService;
import com.cimble.user.dto.UserNotification;
import com.cimble.user.filter.UserNotificationFilter;
import com.cimble.user.management.CimbleUserNotificationManagement;
import com.cimble.user.service.CimbleUserNotificationService;


/**
 * This interface is intended for providing services related to cimble user Location.
 * 
 * @version 1.0
 */
@Path("/api/v1/user/{id}/notification")
@Produces(MediaType.APPLICATION_JSON)

public class DefaultCimbleUserNotificationService extends AbstractUserService<UserNotification, UserNotificationFilter>
implements CimbleUserNotificationService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleUserNotificationService.class);
	
	private CimbleUserNotificationManagement cimbleUserNotificationManagement;

	public DefaultCimbleUserNotificationService(CimbleUserNotificationManagement cimbleUserNotificationManagement) {
		super(UserNotification.class, UserNotificationFilter.class);
		this.cimbleUserNotificationManagement = cimbleUserNotificationManagement;
	}

	@Override
	protected Response save(UserNotification dto, String id) throws Exception {
		dto.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserNotificationManagement.save(dto));
	}
	
	@Override
	protected Response update(UserNotification dto, UserNotificationFilter filter,
			String id) throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserNotificationManagement.update(dto, filter));
	}

	@Override
	protected Response list(UserNotificationFilter filter, Paging paging, String id)
			throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserNotificationManagement.list(filter, paging));
	}

	@Override
	protected Response findById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response deleteById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response delete(UserNotificationFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
