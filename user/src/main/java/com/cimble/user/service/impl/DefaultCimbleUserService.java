package com.cimble.user.service.impl;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.service.AbstractService;
import com.cimble.user.dto.User;
import com.cimble.user.filter.UserFilter;
import com.cimble.user.management.CimbleUserManagement;
import com.cimble.user.service.CimbleUserService;
import com.codahale.metrics.annotation.Timed;

/**
 * This interface is intended for providing services related to cimble admin.
 * 
 * @version 1.0
 */
@Path("/api/v1/user/")
@Produces(MediaType.APPLICATION_JSON)

public class DefaultCimbleUserService extends AbstractService<User, UserFilter>
implements CimbleUserService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleUserService.class);
	
	private CimbleUserManagement cimbleUserManagement;

	public DefaultCimbleUserService(CimbleUserManagement cimbleUserManagement) {
		super(User.class, UserFilter.class);
		this.cimbleUserManagement = cimbleUserManagement;
	}

	@Override
	@Timed(name = "Add productControls#timer")
	protected Response save(User user) throws CimbleException {
		return onSuccess(cimbleUserManagement.saveUser(user));
	}

	@Override
	@Timed(name = "Update productControls#timer")
	protected Response update(User user, UserFilter filter,String id)
			throws CimbleException {
		filter.setId(id);
		return onSuccess(cimbleUserManagement.updateUser(user,filter));
	}

	@Override
	@Timed(name = "Delete productControls#timer")
	public Response deleteById(String user_id ) throws CimbleException {
		throw new UnsupportedOperationException();
	}

	@Override
	@Timed(name = "FindById rule#timer")
	public Response findById(String username) throws CimbleException, InstantiationException, IllegalAccessException {
		System.out.println(username);
		UserFilter filter = UserFilter.class.newInstance();
		filter.setUser_name(username);
		return onSuccess(cimbleUserManagement.findUserOne(filter));
	}

	@Override
	@Timed(name = "List productControls#timer")
	protected Response list(UserFilter filter, Paging paging) throws Exception {
		LOGGER.info("Fetching all product_controls");
		return onSuccess(cimbleUserManagement.listUser(filter, paging));
	}


	
	@Override
	protected Response delete(UserFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
