package com.cimble.user.service.impl;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.paging.Paging;
import com.cimble.service.AbstractUserService;
import com.cimble.user.dto.UserLocation;
import com.cimble.user.filter.UserLocationFilter;
import com.cimble.user.management.CimbleUserLocationManagement;
import com.cimble.user.service.CimbleUserLocationService;

/**
 * This interface is intended for providing services related to cimble user Location.
 * 
 * @version 1.0
 */
@Path("/api/v1/user/{id}/location")
@Produces(MediaType.APPLICATION_JSON)

public class DefaultCimbleUserLocationService extends AbstractUserService<UserLocation, UserLocationFilter>
implements CimbleUserLocationService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleUserLocationService.class);
	
	private CimbleUserLocationManagement cimbleUserLocationManagement;

	public DefaultCimbleUserLocationService(CimbleUserLocationManagement cimbleUserLocationManagement) {
		super(UserLocation.class, UserLocationFilter.class);
		this.cimbleUserLocationManagement = cimbleUserLocationManagement;
	}

	@Override
	protected Response save(UserLocation dto, String id) throws Exception {
		dto.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserLocationManagement.save(dto));
	}

	
	
	@Override
	protected Response update(UserLocation dto, UserLocationFilter filter,
			String id) throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserLocationManagement.update(dto, filter));
	}

	
	
	@Override
	protected Response list(UserLocationFilter filter, Paging paging, String id)
			throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserLocationManagement.list(filter, paging));
	}

	@Override
	protected Response findById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response deleteById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response delete(UserLocationFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	

}
