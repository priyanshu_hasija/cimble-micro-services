package com.cimble.user.service.impl;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.paging.Paging;
import com.cimble.service.AbstractUserService;
import com.cimble.user.dto.UserBilling;
import com.cimble.user.filter.UserBillingFilter;
import com.cimble.user.management.CimbleUserBillingManagement;
import com.cimble.user.service.CimbleUserBillingService;

/**
 * This interface is intended for providing services related to cimble user billling.
 * 
 * @version 1.0
 */
@Path("/api/v1/user/{id}/billing")
@Produces(MediaType.APPLICATION_JSON)

public class DefaultCimbleUserBillingService extends AbstractUserService<UserBilling, UserBillingFilter>
implements CimbleUserBillingService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleUserBillingService.class);
	
	private CimbleUserBillingManagement cimbleUserBillingManagement;

	public DefaultCimbleUserBillingService(CimbleUserBillingManagement cimbleUserBillingManagement) {
		super(UserBilling.class, UserBillingFilter.class);
		this.cimbleUserBillingManagement = cimbleUserBillingManagement;
	}


	@Override
	protected Response update(UserBilling dto, UserBillingFilter filter,
			String id) throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserBillingManagement.update(dto, filter));
	}

	@Override
	protected Response list(UserBillingFilter filter, Paging paging, String id)
			throws Exception {
		// TODO Auto-generated method stub
		filter.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserBillingManagement.list(filter, paging));
	}

	@Override
	protected Response findById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response deleteById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response delete(UserBillingFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected Response save(UserBilling dto, String id) throws Exception {
		dto.setUser_id(Integer.parseInt(id));
		return onSuccess(cimbleUserBillingManagement.save(dto));
	}

	

}
