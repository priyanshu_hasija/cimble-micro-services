package com.cimble.utils;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * This class is intended for providing map functions
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public abstract class MapUtils {

	/**
	 * This function is responsible for retrieving a string value from a map
	 * 
	 * @param map
	 * @param key
	 * @param defaultValue
	 * @return defaultValue if string is empty
	 */
	public static String getStringValue(Map<String, Object> map, String key,
			String defaultValue) {
		if (map != null && map.containsKey(key) && map.get(key) != null) {
			String value = String.valueOf(map.get(key));
			return StringUtils.isEmpty(value) ? defaultValue : value;
		}
		return defaultValue;
	}

	public static Boolean getBooleanValue(Map<String, Object> map, String key,
			Boolean defaultValue) {
		if (map != null && map.containsKey(key) && map.get(key) != null) {
			String value = String.valueOf(map.get(key));
			return value == null ? defaultValue : Boolean.valueOf(value);
		}
		return defaultValue;
	}

	public static Integer getIntegerValue(Map<String, Object> map, String key,
			Integer defaultValue) {
		if (map != null && map.containsKey(key) && map.get(key) != null) {
			String value = String.valueOf(map.get(key));
			return value == null ? defaultValue : Integer.valueOf(value);
		}
		return defaultValue;
	}

	public static Long getLongValue(Map<String, Object> map, String key,
			Long defaultValue) {
		if (map != null && map.containsKey(key) && map.get(key) != null) {
			String value = String.valueOf(map.get(key));
			return value == null ? defaultValue : Long.valueOf(value);
		}
		return defaultValue;
	}

	public static Double getDoubleValue(Map<String, Object> map, String key,
			Double defaultValue) {
		if (map != null && map.containsKey(key) && map.get(key) != null) {
			String value = String.valueOf(map.get(key));
			return value == null ? defaultValue : Double.valueOf(value);
		}
		return defaultValue;
	}

}
