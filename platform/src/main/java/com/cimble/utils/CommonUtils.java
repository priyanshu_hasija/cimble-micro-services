package com.cimble.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

/**
 * This class is intended for providing application wide common functions
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public abstract class CommonUtils {

	private static final int BITMASK = 0X000000FF;
	private static final int SIXTEEN = 16;

	/**
	 * This function is responsible for providing MD5 hash
	 * 
	 * @param input
	 * @return hash
	 * @throws SecurityException
	 * @throws Security
	 */
	public static String getMD5Hash(String input) throws SecurityException {
		byte[] data = input.getBytes();
		String hex = "";
		char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new SecurityException(e);
		}
		md.update(data, 0, data.length);
		byte[] b = md.digest();
		int msb;
		int lsb = 0;
		for (int i = 0; i < b.length; i++) {
			msb = (b[i] & BITMASK) / SIXTEEN;
			lsb = (b[i] & BITMASK) % SIXTEEN;
			hex = hex + hexChars[msb] + hexChars[lsb];
		}
		return hex;
	}

}
