package com.cimble.exception;

/**
 * This class is base class for all exceptions.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public class CimbleException extends Exception {
	private static final long serialVersionUID = 123489239L;

	public CimbleException() {
		super();
	}

	public CimbleException(String message) {
		super(message);
	}

	public CimbleException(Throwable cause) {
		super(cause);
	}

	public CimbleException(String message, Throwable cause) {
		super(message, cause);
	}

}
