package com.cimble.exception;

/**
 * This class is responsible for handling transformation exceptions.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public class TransformationException extends CimbleException {
	private static final long serialVersionUID = 1969869L;

	public TransformationException() {
	}

	public TransformationException(String message) {
		super(message);
	}

	public TransformationException(Throwable cause) {
		super("SecurityException exception", cause);
	}

	public TransformationException(String message, Throwable cause) {
		super(message, cause);
	}

}
