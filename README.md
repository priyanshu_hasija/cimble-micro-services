# README #

This README would normally document whatever steps are necessary to set up and run cimble-micro-services.
Assumptions:

1. Name of working directory is 'Demo'
2. Working machine is local i.e name is 'localhost'
3. Absolute path of 'Demo' is '/home/user/Demo'.

#######################
Prerequisites:
 
1. Java version 1.8 or greater must be installed on machine.
2. Initservice must be present in path '/home/user/Demo'.

#######################
Running Cimble-Micro services:

1. Traverse to path : /home/user/Demo/cimble-services/<service-name>.For e.g. /home/user/Demo/cimble-services/admin
2. Set valid parameters in cimbleAdmin.yml:

        ─ applicationConnectors:
                  - type: http
                    port: <PORT_NUMBER>
        ─ currentLogFilename: <PATH FOR LOG FILE>

3. For running API,

	java -jar <PATH OF ADMIN JAR FILE> server /home/user/Demo/cimble-services/<service-name>/<yml-file>