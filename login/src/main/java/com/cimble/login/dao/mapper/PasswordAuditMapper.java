package com.cimble.login.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.login.dto.PasswordAudit;
import com.cimble.login.filter.PasswordAuditFilter;
import com.cimble.mapper.RequestMapper;

public class PasswordAuditMapper implements ResultSetMapper<PasswordAudit>,
RequestMapper<PasswordAudit, PasswordAuditFilter> {

	private static final String TABLE_NAME = "SUBSCRIBER_TOKEN";
	private static final String ID = "ID";
	private static final String USER_NAME ="USERNAME";
	private static final String IP_ADDRESS = "IP_ADDRESS";
	private static final String TOKEN = "TOKEN";
	private static final String TIMESTAMP = "TIMESTAMP";
	
	@Override
	public Map<String, Object> convertToDao(PasswordAudit dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (dto.getId() != null)
			map.put(ID, dto.getId());
		if (StringUtils.isNotBlank(dto.getUsername()))
			map.put(USER_NAME, dto.getUsername());
		if (StringUtils.isNotBlank(dto.getIpAddress()))
			map.put(IP_ADDRESS, dto.getIpAddress());
		if (StringUtils.isNotBlank(dto.getToken()))
		{
			//Condition added for deleting token after password reset
			if(dto.getToken().equals("inactive"))
			{
				map.put(TOKEN, "");
			}else{
			map.put(TOKEN, dto.getToken());
		
			}
		}
		if (dto.getCreatedOn() != null)
			map.put(TIMESTAMP, dto.getCreatedOn());
		if (dto.getCreatedOn() != null)
			map.put(PasswordAudit.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(PasswordAudit.MODIFIED_ON, dto.getModifiedOn());
		
		return map;
	}

	@Override
	public Map<String, Object> convertToDao(PasswordAuditFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			if(StringUtils.isNotBlank(filter.getToken()))
				map.put(TOKEN, filter.getToken());
			
			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public PasswordAudit map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		PasswordAudit obj = new PasswordAudit();

		obj.setId(rs.getString(ID));
		obj.setIpAddress(rs.getString(IP_ADDRESS));
		obj.setUsername(rs.getString(USER_NAME));
		obj.setToken(rs.getString(TOKEN));
		obj.setCreatedOn(rs.getLong(TIMESTAMP));
		
		return obj;
	}

}
