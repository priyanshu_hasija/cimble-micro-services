package com.cimble.login.dao;

import java.util.Date;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.dao.AbstractRelationalDao;
import com.cimble.login.dao.mapper.PasswordAuditMapper;
import com.cimble.login.dto.PasswordAudit;
import com.cimble.login.filter.PasswordAuditFilter;

@RegisterMapper(PasswordAuditMapper.class)
@UseStringTemplate3StatementLocator
public abstract class CimblePasswordAuditDAO extends
AbstractRelationalDao<PasswordAudit, PasswordAuditFilter, PasswordAuditMapper> {

	public CimblePasswordAuditDAO() throws Exception {
		super(PasswordAuditMapper.class);
	}

	public PasswordAudit validateToken(PasswordAuditFilter filter) {
		Long currentTime = new Date().getTime() - (7*25*60*60*1000);
		System.out.println(currentTime);
		return findOne("SUBSCRIBER_TOKEN", WHERE + "token ='"+filter.getToken()+"' and timestamp >= "+currentTime);
	}

}
