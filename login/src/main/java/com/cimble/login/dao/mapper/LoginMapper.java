package com.cimble.login.dao.mapper;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Login}
 * .
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.login.dto.Login;
import com.cimble.login.filter.LoginFilter;
import com.cimble.mapper.RequestMapper;

public class LoginMapper implements ResultSetMapper<Login>,
		RequestMapper<Login, LoginFilter> {

	private static final String TABLE_NAME = "LOGIN";
	private static final String ID = "ID";
	private static final String USER_NAME ="USERNAME";
	private static final String ROLE = "ROLE";
	private static final String PASSWORD = "PASSWORD";
	
	@Override
	public Login map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		Login obj = new Login();

		obj.setId(rs.getString(ID));
		obj.setUserName(rs.getString(USER_NAME));
		obj.setRole(rs.getString(ROLE));
		obj.setPassword(rs.getString(PASSWORD));
		
		return obj;
	}
	
	@Override
	public Map<String, Object> convertToDao(Login dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (dto.getId() != null)
			map.put(ID, dto.getId());
		if (StringUtils.isNotBlank(dto.getUserName()))
			map.put(USER_NAME, dto.getUserName());
		if (StringUtils.isNotBlank(dto.getRole()))
			map.put(ROLE, dto.getRole());
		if (StringUtils.isNotBlank(dto.getPassword()))
			map.put(PASSWORD, dto.getPassword());
		if (dto.getCreatedOn() != null)
			map.put(Login.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(Login.MODIFIED_ON, dto.getModifiedOn());
		
		return map;
	}
	
	@Override
	public Map<String, Object> convertToDao(LoginFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			if(filter.getId() != null)
				map.put(ID, filter.getId());
			
			if(StringUtils.isNotBlank(filter.getUserName()))
				map.put(USER_NAME, filter.getUserName());

			if(StringUtils.isNotBlank(filter.getPassword()))
				map.put(PASSWORD, filter.getPassword());

			return map;
		}
	}
	@Override
	public String getTableName() {
		return TABLE_NAME;
	}
}
