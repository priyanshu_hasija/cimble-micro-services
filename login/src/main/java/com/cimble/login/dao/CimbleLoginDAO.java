package com.cimble.login.dao;

import java.util.Date;
import java.util.StringJoiner;

/**
 * This interface is intended for providing interactions with User table.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.constants.Punctuation;
import com.cimble.dao.AbstractRelationalDao;
import com.cimble.exception.CimbleException;
import com.cimble.login.dao.mapper.LoginMapper;
import com.cimble.login.dto.Login;
import com.cimble.login.filter.LoginFilter;


@RegisterMapper(LoginMapper.class)
@UseStringTemplate3StatementLocator
public abstract class CimbleLoginDAO extends
			AbstractRelationalDao<Login, LoginFilter, LoginMapper> {
	

	public CimbleLoginDAO() throws Exception {
		super(LoginMapper.class);
	}

	public int resetPassword(Login login, String token) throws CimbleException {
		 StringJoiner setFields = null;
		 if(login.getModifiedOn() == null)
		 {
			 login.setModifiedOn(new Date().getTime());
		 }
		try {
			setFields = join(Punctuation.COMMA.toString(), LoginMapper.class.newInstance().convertToDao(login));
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	        if (setFields.length() == 0) {
	            return 0;
	        }
	        
	        return update("LOGIN",setFields.toString(),
	        		"where username in (select username from SUBSCRIBER_TOKEN where token = '"+token+"')");
	}



}






