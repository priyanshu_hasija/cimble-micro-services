package com.cimble.login.config;

/**
 * This class is intended for holding all the configurations injected from the external *.yml file.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */

import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cimble.config.AbstractBootstrapConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CimbleLoginBootstrapConfiguration extends AbstractBootstrapConfiguration {

    @Valid
    @NotNull
    @JsonProperty
    private final DataSourceFactory dataSourceFactory = new DataSourceFactory();


    public DataSourceFactory getDataSourceFactory() {
        return dataSourceFactory;
    }


}