package com.cimble.login.filter;

/**
 * This class is responsible for holding generic fields to be returned in
 * response.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */

import com.cimble.filter.SearchFilter;

public class LoginFilter extends SearchFilter {
private static final long serialVersionUID = -9155632280790356699L;
	
	private String UserName;
	private String Password;
	private Integer user_id;
	
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	

	
	
	
}
