package com.cimble.login.bootstrap;

import org.skife.jdbi.v2.DBI;

import com.cimble.bootstrap.AbstractBootstrap;
import com.cimble.login.config.CimbleLoginBootstrapConfiguration;
import com.cimble.login.dao.CimbleLoginDAO;
import com.cimble.login.dao.CimblePasswordAuditDAO;
import com.cimble.login.management.CimbleLoginManagement;
import com.cimble.login.management.CimblePasswordAuditManagement;
import com.cimble.login.management.DefaultCimbleLoginManagement;
import com.cimble.login.management.DefaultCimblePasswordAuditManagement;
import com.cimble.login.service.impl.DefaultCimbleLoginService;
import com.cimble.login.service.impl.DefaultPasswordAuditService;

/**
* This class is intended for starting the application. It registers service, ui
* & db migration modules.
* 
* @author Priyanshu Hasija
* @version 1.0
*/

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class CimbleLoginBootstrap extends
		AbstractBootstrap<CimbleLoginBootstrapConfiguration> {

	public static final String ID = "cimble";
	private CimbleLoginManagement cimbleLoginManagement;
	private CimblePasswordAuditManagement cimblePasswordAuditManagement;
	public static void main(String[] args) throws Exception {
		new CimbleLoginBootstrap().run(args);
	}
	@Override
	public void initializeConfig(
			Bootstrap<CimbleLoginBootstrapConfiguration> bootstrap) {
		bootstrap.addBundle(new MigrationsBundle<CimbleLoginBootstrapConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(CimbleLoginBootstrapConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
	}

	@Override
	public void runBootstrap(CimbleLoginBootstrapConfiguration configuration,
			Environment environment) throws Exception {
		DBI dbi = new DBI(configuration.getDataSourceFactory().getUrl(),
				configuration.getDataSourceFactory().getUser(), configuration
						.getDataSourceFactory().getPassword());

		CimbleLoginDAO ingestDao = dbi.onDemand(CimbleLoginDAO.class);

		cimbleLoginManagement = new DefaultCimbleLoginManagement(ingestDao);
		
		environment.jersey().register(
				new DefaultCimbleLoginService(cimbleLoginManagement));
		
		CimblePasswordAuditDAO cimblePasswordAuditDAO = dbi.onDemand(CimblePasswordAuditDAO.class);

		cimblePasswordAuditManagement = new DefaultCimblePasswordAuditManagement(cimblePasswordAuditDAO);
		
		environment.jersey().register(
				new DefaultPasswordAuditService(cimblePasswordAuditManagement));
	}
		@Override
		public String getId() {
			return ID;
		}

	}

	