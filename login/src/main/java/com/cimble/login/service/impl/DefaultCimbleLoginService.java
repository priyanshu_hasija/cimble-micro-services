package com.cimble.login.service.impl;

/**
 * This interface is intended for providing services related to cimble Login.
 * 
 * @version 1.0
 */

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.exception.CimbleException;
import com.cimble.login.dto.Login;
import com.cimble.login.filter.LoginFilter;
import com.cimble.login.management.CimbleLoginManagement;
import com.cimble.login.service.CimbleLoginService;
import com.cimble.paging.Paging;
import com.cimble.service.AbstractBulkService;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/api/v1")
@Produces(MediaType.APPLICATION_JSON)
public class DefaultCimbleLoginService extends
AbstractBulkService<Login, LoginFilter> implements CimbleLoginService {

	final static Logger LOGGER = LoggerFactory
			.getLogger(DefaultCimbleLoginService.class);

	private CimbleLoginManagement cimbleLoginManagement;

	public DefaultCimbleLoginService(CimbleLoginManagement cimbleLoginManagement) {
		super(Login.class, LoginFilter.class);
		this.cimbleLoginManagement = cimbleLoginManagement;
	}

	@POST
	@Path("/signin")
	public Response login(String data) throws CimbleException {

		try {
			/* System.out.println("raw data:"+data); */
			LoginFilter filter = LoginFilter.class.newInstance();
			ObjectMapper mapper = new ObjectMapper();
			Login ln = mapper.readValue(data, Login.class);
			String pass = ln.getPassword();
			String user = ln.getUserName();
			if (StringUtils.isNotBlank(pass) && StringUtils.isNotBlank(user)){
				filter.setUserName(user);
				filter.setPassword(pass);
				Login checkLogin = cimbleLoginManagement.findUser(filter);
				if (checkLogin != null) {
					return onSuccess(checkLogin);
				} else {
					return onError(Status.UNAUTHORIZED.getStatusCode());
				}
			}
			else{
				return onError(Status.NOT_ACCEPTABLE.getStatusCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in save()", e);
		}
	}

	@PUT
	@Path("/reset/password/{token}")
	public Response resetPassword(@PathParam("token") String token,String data) throws CimbleException {

		try {
			/* System.out.println("raw data:"+data); */
			ObjectMapper mapper = new ObjectMapper();
			Login login = mapper.readValue(data, Login.class);
			return onSuccess(cimbleLoginManagement.resetPassword(login,token));
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in save()", e);
		}
	}

	@GET
	@Path("/sendMail")
	public Response sendMail(@QueryParam("email") String email,@QueryParam("url") String url) throws CimbleException {
		String to = email;//change accordingly

		// Sender's email ID needs to be mentioned
		String from = "testCimble@gmail.com";//change accordingly
		final String username = "testCimble@gmail.com";//change accordingly
		final String password = "testCimble@123";//change accordingly

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "smtp.gmail.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		props.put("java.net.preferIPv4Stack",true);

		// Get the Session object.
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));

			// Set Subject: header field
			message.setSubject("Cimble Password Recovery");
			String body = "Dear User,<br /> We received a request for password change at Cimble.com.<br /><a href=\""+url+"\" target=\"_blank\">Go to this page</a> to set your new password. The link will be active for seven days.<br /><br />Regards,<br />The Cimble team";

			// Now set the actual message
			message.setContent(body,"text/html");

			// Send message
			Transport.send(message);

			System.out.println("sent mail successfully");

			return onSuccess(Status.ACCEPTED.getStatusCode());

		} catch (MessagingException e) {
			e.printStackTrace();
			throw new CimbleException("Error in sending Mail()", e);
		}
	}

	@Override
	@Timed(name = "Add #timer")
	protected Response save(Login login) throws CimbleException {
		return onSuccess(cimbleLoginManagement.signUpUser(login));
	}

	@Override
	@Timed(name = "Update #timer")
	protected Response update(Login login, LoginFilter filter,String username) throws CimbleException {
		filter.setUserName(username);
		return onSuccess(cimbleLoginManagement.update(login,filter));
	}



	@Override
	protected Response bulkSave(List<Login> dtos) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response bulkUpdate(List<Login> dtos) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response bulkDelete(List<LoginFilter> filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response list(LoginFilter filter, Paging paging) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response findById(String username) throws Exception {
		// TODO Auto-generated method stub
		LoginFilter filter = new LoginFilter();
		filter.setUserName(username);
		Login checkLogin = cimbleLoginManagement.findUser(filter);
		if (checkLogin != null) {
			return onSuccess(checkLogin);
		} else {
			return onError(Status.NO_CONTENT.getStatusCode());
		}
	}

	@Override
	protected Response deleteById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response delete(LoginFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}