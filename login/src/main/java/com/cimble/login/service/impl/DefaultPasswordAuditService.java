package com.cimble.login.service.impl;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cimble.exception.CimbleException;
import com.cimble.login.dto.PasswordAudit;
import com.cimble.login.filter.PasswordAuditFilter;
import com.cimble.login.management.CimblePasswordAuditManagement;
import com.cimble.login.service.PasswordAuditService;
import com.cimble.paging.Paging;
import com.cimble.service.AbstractBulkService;


@Path("/api/v1/token")
@Produces(MediaType.APPLICATION_JSON)
public class DefaultPasswordAuditService extends
AbstractBulkService<PasswordAudit, PasswordAuditFilter> implements PasswordAuditService {

	private CimblePasswordAuditManagement cimblePasswordAuditManagement; 
	
	public DefaultPasswordAuditService(CimblePasswordAuditManagement cimblePasswordAuditManagement) {
		super(PasswordAudit.class, PasswordAuditFilter.class);
		this.cimblePasswordAuditManagement = cimblePasswordAuditManagement;
		// TODO Auto-generated constructor stub
	}
	
	@GET
	@Path("/validate/{token}")
	public Response getPasswordAuditDetails(@PathParam("token") String token) throws CimbleException {

		try {
			/* System.out.println("raw data:"+data); */
			PasswordAuditFilter filter = PasswordAuditFilter.class.newInstance();
			filter.setToken(token);
			return onSuccess(cimblePasswordAuditManagement.getPasswordAuditDetails(filter));
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in validateToken()", e);
		}
	}
	
	@Override
	protected Response bulkSave(List<PasswordAudit> dtos) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response bulkUpdate(List<PasswordAudit> dtos) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response bulkDelete(List<PasswordAuditFilter> filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response save(PasswordAudit passwordAudit) throws Exception {
			return onSuccess(cimblePasswordAuditManagement.addToken(passwordAudit));
	}

	@Override
	protected Response update(PasswordAudit dto, PasswordAuditFilter filter, String token) throws Exception {
		dto.setToken("inactive");
		filter.setToken(token);
		return onSuccess(cimblePasswordAuditManagement.deleteToken(dto,filter));
	}

	@Override
	protected Response list(PasswordAuditFilter filter, Paging paging) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response findById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response deleteById(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Response delete(PasswordAuditFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}
