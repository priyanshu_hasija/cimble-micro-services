package com.cimble.login.dto;

import com.cimble.dto.AbstractAuditDTO;
 
 
public class Login extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L;
	
	private String userName;
	private String role;
	private String password;
	
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
