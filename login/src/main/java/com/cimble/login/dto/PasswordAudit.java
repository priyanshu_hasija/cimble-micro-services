package com.cimble.login.dto;

import com.cimble.dto.AbstractAuditDTO;

/**
 * @author Priyanshu
 *
 */
public class PasswordAudit extends AbstractAuditDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9174312652594090980L;
	private String username;
	private String ipAddress;
	private String token;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
