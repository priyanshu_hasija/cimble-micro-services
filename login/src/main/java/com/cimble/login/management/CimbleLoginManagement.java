package com.cimble.login.management;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */

import com.cimble.exception.CimbleException;
import com.cimble.login.dto.Login;
import com.cimble.login.filter.LoginFilter;

public interface CimbleLoginManagement {
	
	Login findUser(LoginFilter filter);
	
	Login signUpUser (Login login) throws CimbleException;
	
	Login update (Login login, LoginFilter loginFilter) throws CimbleException;

	Login resetPassword(Login login, String token) throws CimbleException;

}
