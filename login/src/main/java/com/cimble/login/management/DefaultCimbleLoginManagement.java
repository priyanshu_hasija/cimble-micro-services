package com.cimble.login.management;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */

import com.cimble.exception.CimbleException;
import com.cimble.login.dao.CimbleLoginDAO;
import com.cimble.login.dto.Login;
import com.cimble.login.filter.LoginFilter;

public class DefaultCimbleLoginManagement implements CimbleLoginManagement {
	private CimbleLoginDAO cimbleLoginDAO;

	public DefaultCimbleLoginManagement(CimbleLoginDAO cimbleLoginDAO) {
		super();
		this.cimbleLoginDAO = cimbleLoginDAO;
	}

	@Override
	public Login findUser(LoginFilter filter) {
		return cimbleLoginDAO.findOne(filter);
	}
	
	@Override
	public Login signUpUser(Login login) throws CimbleException {
		if(cimbleLoginDAO.save (login) != 0)
		{
			return login;
		}
		return new Login();
	}
	
	@Override
	public Login update(Login login, LoginFilter filter) throws CimbleException {
		System.out.println("jfdsijf;lj"+filter.getId());
		if(cimbleLoginDAO.update(login, filter) != 0)
		{
			return login;
		}
		return new Login();
	}

	@Override
	public Login resetPassword(Login login, String token) throws CimbleException {
		if(cimbleLoginDAO.resetPassword(login, token) != 0)
		{
			return login;
		}
		return new Login();
	}

}
