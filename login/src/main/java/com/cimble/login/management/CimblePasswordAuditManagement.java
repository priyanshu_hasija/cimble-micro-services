package com.cimble.login.management;

import com.cimble.exception.CimbleException;
import com.cimble.login.dto.PasswordAudit;
import com.cimble.login.filter.PasswordAuditFilter;

public interface CimblePasswordAuditManagement {

	PasswordAudit addToken(PasswordAudit passwordAudit) throws CimbleException;

	PasswordAudit getPasswordAuditDetails(PasswordAuditFilter filter) throws CimbleException;

	PasswordAudit deleteToken(PasswordAudit dto, PasswordAuditFilter filter);
}
