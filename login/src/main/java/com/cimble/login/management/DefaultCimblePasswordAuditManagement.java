package com.cimble.login.management;

import com.cimble.exception.CimbleException;
import com.cimble.login.dao.CimblePasswordAuditDAO;
import com.cimble.login.dto.PasswordAudit;
import com.cimble.login.filter.PasswordAuditFilter;

public class DefaultCimblePasswordAuditManagement implements CimblePasswordAuditManagement{

	private CimblePasswordAuditDAO cimblePasswordAuditDAO;
	
	public DefaultCimblePasswordAuditManagement(CimblePasswordAuditDAO cimblePasswordAuditDAO) {
		super();
		this.cimblePasswordAuditDAO = cimblePasswordAuditDAO;
	}
	
	@Override
	public PasswordAudit addToken(PasswordAudit passwordAudit) throws CimbleException {
		if(cimblePasswordAuditDAO.save(passwordAudit) != 0)
		{
			return passwordAudit;
		}
		return new PasswordAudit();
	}

	@Override
	public PasswordAudit getPasswordAuditDetails(PasswordAuditFilter filter) throws CimbleException {
		return cimblePasswordAuditDAO.validateToken(filter);
	}

	@Override
	public PasswordAudit deleteToken(PasswordAudit dto, PasswordAuditFilter filter) {
		if(cimblePasswordAuditDAO.update(dto,filter) != 0)
		{
			return dto;
		}
		return new PasswordAudit();
	}

}