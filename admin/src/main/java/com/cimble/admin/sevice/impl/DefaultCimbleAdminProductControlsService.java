package com.cimble.admin.sevice.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.admin.dto.ProductControls;
import com.cimble.admin.filter.ProductControlsFilter;
import com.cimble.admin.management.CimbleAdminProductControlsManagement;
import com.cimble.admin.service.CimbleAdminProductControlsService;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.service.AbstractBulkService;
import com.codahale.metrics.annotation.Timed;

/**
 * This interface is intended for providing services related to cimble admin.
 * 
 * @version 1.0
 */
@Path("/api/v1/admin/product/controls")
@Produces(MediaType.APPLICATION_JSON)

public class DefaultCimbleAdminProductControlsService extends AbstractBulkService<ProductControls, ProductControlsFilter>
implements CimbleAdminProductControlsService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleAdminProductControlsService.class);
	
	private CimbleAdminProductControlsManagement cimbleAdminProductControlsManagement;

	public DefaultCimbleAdminProductControlsService(CimbleAdminProductControlsManagement cimbleAdminProductControlsManagement) {
		super(ProductControls.class, ProductControlsFilter.class);
		this.cimbleAdminProductControlsManagement = cimbleAdminProductControlsManagement;
	}

	@Override
	@Timed(name = "Add productControls#timer")
	protected Response save(ProductControls productControls) throws CimbleException {
		return onSuccess(cimbleAdminProductControlsManagement.saveProductControls(productControls));
	}

	@Override
	@Timed(name = "Update productControls#timer")
	protected Response update(ProductControls productControls, ProductControlsFilter filter,String id)
			throws CimbleException {
		filter.setId(id);
		return onSuccess(cimbleAdminProductControlsManagement.updateProductControls(productControls,filter));
	}

	@Override
	@Timed(name = "Delete productControls#timer")
	public Response deleteById(String product_id) throws CimbleException {
		throw new UnsupportedOperationException();
	}

	@Override
	@Timed(name = "FindById rule#timer")
	public Response findById(String id) throws CimbleException, InstantiationException, IllegalAccessException {
		ProductControlsFilter filter = ProductControlsFilter.class.newInstance();
		filter.setId(id);
		return onSuccess(cimbleAdminProductControlsManagement.findProductControlsOne(filter));
	}

	@Override
	@Timed(name = "List productControls#timer")
	protected Response list(ProductControlsFilter filter, Paging paging) throws Exception {
		LOGGER.info("Fetching all product_controls");
		return onSuccess(cimbleAdminProductControlsManagement.listProductControls(filter, paging));
	}

	@Override
	protected Response bulkSave(List<ProductControls> dtos) throws Exception {
		List<ProductControls> resDtos = new ArrayList<ProductControls>();
		for (ProductControls dto : dtos) {
			ProductControls resDto = cimbleAdminProductControlsManagement.saveProductControls(dto);
			if (resDto != null) {
				resDtos.add(resDto);
			}
		}
		return onSuccess(resDtos);
	}

	@Override
	protected Response bulkUpdate(List<ProductControls> dtos)
			throws Exception {
		return null;
	}

	@Override
	protected Response bulkDelete(List<ProductControlsFilter> filters) throws Exception {
		List<Integer> statusList = new ArrayList<Integer>();
		
		
		for (ProductControlsFilter filter : filters) {
			int status = cimbleAdminProductControlsManagement.deleteProductControls(filter);
			statusList.add(status);
			}
		return onSuccess(statusList);
	}
	
	@Override
	protected Response delete(ProductControlsFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
