package com.cimble.admin.sevice.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cimble.admin.dto.ProductEvent;
import com.cimble.admin.filter.ProductEventFilter;
import com.cimble.admin.management.CimbleAdminProductEventManagement;
import com.cimble.admin.service.CimbleAdminProductEventService;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.service.AbstractBulkService;
import com.codahale.metrics.annotation.Timed;

/**
 * This interface is intended for providing services related to cimble admin product api.
 * 
 * @version 1.0
 */


@Path("/api/v1/admin/product/events")
@Produces(MediaType.APPLICATION_JSON)
public class DefaultCimbleAdminProductEventService extends AbstractBulkService<ProductEvent, ProductEventFilter>
		implements CimbleAdminProductEventService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleAdminProductEventService.class);

	private CimbleAdminProductEventManagement cimbleAdminProductEventManagement;

	public DefaultCimbleAdminProductEventService(CimbleAdminProductEventManagement cimbleAdminProductEventManagement) {
		super(ProductEvent.class, ProductEventFilter.class);
		this.cimbleAdminProductEventManagement = cimbleAdminProductEventManagement;
	}

	@Override
	@Timed(name = "Add ProductEvent#timer")
	protected Response save(ProductEvent productEvent) throws CimbleException {
		return onSuccess(cimbleAdminProductEventManagement.saveProductEvent(productEvent));
	}

	@Override
	@Timed(name = "Update productEvent#timer")
	protected Response update(ProductEvent productEvent, ProductEventFilter filter,String id)
			throws CimbleException {
		filter.setId(id);;
		return onSuccess(cimbleAdminProductEventManagement.updateProductEvent(productEvent,filter));
	}

	@Override
	@Timed(name = "Delete productEvent#timer")
	public Response deleteById(String product_id) throws CimbleException {
		throw new UnsupportedOperationException();
	}

	@Override
	@Timed(name = "FindProductEventById rule#timer")
	public Response findById(String id) throws CimbleException, InstantiationException, IllegalAccessException {
		ProductEventFilter filter = ProductEventFilter.class.newInstance();
		filter.setId(id);
		return onSuccess(cimbleAdminProductEventManagement.findOneProductEvent(filter));
	}

	@Override
	@Timed(name = "List productEvent#timer")
	protected Response list(ProductEventFilter filter, Paging paging) throws Exception {
		LOGGER.info("Fetching all product_events");
		return onSuccess(cimbleAdminProductEventManagement.listProductsEvents(filter, paging));
	}

	@Override
	protected Response bulkSave(List<ProductEvent> dtos) throws Exception {
		List<ProductEvent> resDtos = new ArrayList<ProductEvent>();
		for (ProductEvent dto : dtos) {
			ProductEvent resDto = cimbleAdminProductEventManagement.saveProductEvent(dto);
			if (resDto != null) {
				resDtos.add(resDto);
			}
		}
		return onSuccess(resDtos);
	}

	@Override
	protected Response bulkUpdate(List<ProductEvent> dtos)
			throws Exception {
		return null;
	}

	@Override
	protected Response bulkDelete(List<ProductEventFilter> filters) throws Exception {
		List<Integer> statusList = new ArrayList<Integer>();
		
		
		for (ProductEventFilter filter : filters) {
			int status = cimbleAdminProductEventManagement.deleteProductEvent(filter);
			statusList.add(status);
			}
		return onSuccess(statusList);
	}
	
	@Override
	protected Response delete(ProductEventFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
