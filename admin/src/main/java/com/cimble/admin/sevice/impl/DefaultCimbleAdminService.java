package com.cimble.admin.sevice.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cimble.admin.dto.Partner;
import com.cimble.admin.filter.PartnerFilter;
import com.cimble.admin.management.CimbleAdminManagement;
import com.cimble.admin.service.CimbleAdminService;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.service.AbstractBulkService;
import com.codahale.metrics.annotation.Timed;

/**
 * This interface is intended for providing services related to cimble admin.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
@Path("/api/v1/partner")
@Produces(MediaType.APPLICATION_JSON)
public class DefaultCimbleAdminService extends AbstractBulkService<Partner, PartnerFilter>
		implements CimbleAdminService {
	
	final static Logger LOGGER = 
			LoggerFactory.getLogger(DefaultCimbleAdminService.class);

	private CimbleAdminManagement cimbleAdminManagement;

	public DefaultCimbleAdminService(CimbleAdminManagement cimbleAdminManagement) {
		super(Partner.class, PartnerFilter.class);
		this.cimbleAdminManagement = cimbleAdminManagement;
	}
	
	@PUT
	@Path("/suspend/{id}")
	public Response suspend(@PathParam("id") String id) throws CimbleException {
		try {
			return onSuccess(cimbleAdminManagement.suspend(id));
		} catch (CimbleException e) {
			throw new CimbleException("Error in suspend()", e);
		}
	}

	@PUT
	@Path("/remove/{id}")
	public Response remove(@PathParam("id") String id) throws CimbleException {
		try {
			return onSuccess(cimbleAdminManagement.remove(id));
		} catch (CimbleException e) {
			throw new CimbleException("Error in remove()", e);
		}
	}
	
	@PUT
	@Path("/token/{id}")
	public Response assignToken(@PathParam("id") String id) throws CimbleException {
		try {
			UUID uuid = UUID.randomUUID();
			String token = uuid.toString();
			return onSuccess(cimbleAdminManagement.assignToken(token,id));
		} catch (CimbleException e) {
			throw new CimbleException("Error in assignToken()", e);
		}
	}

	@Override
	@Timed(name = "Add partner#timer")
	protected Response save(Partner partner) throws CimbleException {
		return onSuccess(cimbleAdminManagement.save(partner));
	}

	@Override
	@Timed(name = "Update partner#timer")
	protected Response update(Partner partner, PartnerFilter filter,String id)
			throws CimbleException {
		filter.setId(id);
		return onSuccess(cimbleAdminManagement.update(partner,filter));
	}

	@Override
	@Timed(name = "Delete partner#timer")
	public Response deleteById(String group_id) throws CimbleException {
		throw new UnsupportedOperationException();
	}

	@Override
	@Timed(name = "FindById rule#timer")
	public Response findById(String id) throws CimbleException, InstantiationException, IllegalAccessException {
		PartnerFilter filter = PartnerFilter.class.newInstance();
		filter.setId(id);
		return onSuccess(cimbleAdminManagement.findOne(filter));
	}

	@Override
	@Timed(name = "List partners#timer")
	protected Response list(PartnerFilter filter, Paging paging) throws Exception {
		LOGGER.info("Fetching all Partners");
		return onSuccess(cimbleAdminManagement.list(filter, paging));
	}

	@Override
	protected Response bulkSave(List<Partner> dtos) throws Exception {
		List<Partner> resDtos = new ArrayList<Partner>();
		for (Partner dto : dtos) {
			Partner resDto = cimbleAdminManagement.save(dto);
			if (resDto != null) {
				resDtos.add(resDto);
			}
		}
		return onSuccess(resDtos);
	}

	@Override
	protected Response bulkUpdate(List<Partner> dtos)
			throws Exception {
		return null;
	}

	@Override
	protected Response bulkDelete(List<PartnerFilter> filters) throws Exception {
		List<Integer> statusList = new ArrayList<Integer>();
		
		
		for (PartnerFilter filter : filters) {
			int status = cimbleAdminManagement.delete(filter);
			statusList.add(status);
			}
		return onSuccess(statusList);
	}
	
	@Override
	protected Response delete(PartnerFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
