package com.cimble.admin.sevice.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.admin.dto.Product;
import com.cimble.admin.filter.ProductFilter;
import com.cimble.admin.management.CimbleAdminProductManagement;
import com.cimble.admin.service.CimbleAdminProductService;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.service.AbstractBulkService;
import com.codahale.metrics.annotation.Timed;

/**
 * This interface is intended for providing services related to cimble admin product api.
 * 
 * @version 1.0
 */


@Path("/api/v1/admin/product")
@Produces(MediaType.APPLICATION_JSON)
public class DefaultCimbleAdminProductService extends AbstractBulkService<Product, ProductFilter>
		implements CimbleAdminProductService {

	final static Logger LOGGER = LoggerFactory.getLogger(DefaultCimbleAdminProductService.class);
	
	private CimbleAdminProductManagement cimbleAdminProductManagement;

	public DefaultCimbleAdminProductService(CimbleAdminProductManagement cimbleAdminProductManagement) {
		super(Product.class, ProductFilter.class);
		this.cimbleAdminProductManagement = cimbleAdminProductManagement;
	}

	@Override
	@Timed(name = "Add Product#timer")
	protected Response save(Product product) throws CimbleException {
		return onSuccess(cimbleAdminProductManagement.saveProduct(product));
	}

	@Override
	@Timed(name = "Update product#timer")
	protected Response update(Product product, ProductFilter filter,String id)
			throws CimbleException {
		filter.setId(id);
		return onSuccess(cimbleAdminProductManagement.updateProduct(product,filter));
	}

	@Override
	@Timed(name = "Delete product#timer")
	public Response deleteById(String group_id) throws CimbleException {
		throw new UnsupportedOperationException();
	}

	@Override
	@Timed(name = "FindProductById rule#timer")
	public Response findById(String id) throws CimbleException, InstantiationException, IllegalAccessException {
		ProductFilter filter = ProductFilter.class.newInstance();
		filter.setPartnerId(Integer.parseInt(id));
		return onSuccess(cimbleAdminProductManagement.findOneProduct(filter));
	}

	@Override
	@Timed(name = "List products#timer")
	protected Response list(ProductFilter filter, Paging paging) throws Exception {
		LOGGER.info("Fetching all products");
		return onSuccess(cimbleAdminProductManagement.listProducts(filter, paging));
	}

	@Override
	protected Response bulkSave(List<Product> dtos) throws Exception {
		List<Product> resDtos = new ArrayList<Product>();
		for (Product dto : dtos) {
			Product resDto = cimbleAdminProductManagement.saveProduct(dto);
			if (resDto != null) {
				resDtos.add(resDto);
			}
		}
		return onSuccess(resDtos);
	}

	@Override
	protected Response bulkUpdate(List<Product> dtos)
			throws Exception {
		return null;
	}

	@Override
	protected Response bulkDelete(List<ProductFilter> filters) throws Exception {
		List<Integer> statusList = new ArrayList<Integer>();
		
		
		for (ProductFilter filter : filters) {
			int status = cimbleAdminProductManagement.deleteProduct(filter);
			statusList.add(status);
			}
		return onSuccess(statusList);
	}
	
	@Override
	protected Response delete(ProductFilter filter) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@PUT
	@Path("/clone")
	public Response clone(@QueryParam("current_product_id") Integer curr_id,@QueryParam("new_product_id") Integer new_id) throws CimbleException {
		try {
			return onSuccess(cimbleAdminProductManagement.clone(curr_id,new_id));
		} catch (CimbleException e) {
			throw new CimbleException("Error in suspend()", e);
		}
	}
	
//	@GET
//	@Path("/list/{id}")
//	public Response productsByPartner(@PathParam("id") String id,@Context HttpServletRequest request) throws CimbleException, InstantiationException, IllegalAccessException {
//		try {
//			Map<String, String> map = new HashMap<>();
//			Enumeration<String> params = request.getParameterNames();
//			while (params.hasMoreElements()) {
//				String param = params.nextElement();
//				map.put(param, request.getParameter(param));
//			}
//
//			ObjectMapper mapper = new ObjectMapper();
//			ProductFilter filter = ProductFilter.class.newInstance();
//			filter.setGroup_id(Integer.parseInt(id));
//			return onSuccess(cimbleAdminProductManagement.productsByPartner(filter,mapper.convertValue(map, Paging.class)));
//		} catch (CimbleException e) {
//			throw new CimbleException("Error in productsByPartner()", e);
//		}
//	}

}
