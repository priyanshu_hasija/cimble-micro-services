package com.cimble.admin.management;

import java.util.List;
import com.cimble.admin.dao.CimbleAdminProductEventDAO;
import com.cimble.admin.dto.ProductEvent;
import com.cimble.admin.filter.ProductEventFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */
public class DefaultCimbleAdminProductEventManagement implements CimbleAdminProductEventManagement{
	
	private CimbleAdminProductEventDAO cimbleAdminProductEventDao;

	public DefaultCimbleAdminProductEventManagement(CimbleAdminProductEventDAO cimbleAdminProductEventDao) {
		super();
		this.cimbleAdminProductEventDao = cimbleAdminProductEventDao;
	}

	@Override
	public ProductEvent saveProductEvent(ProductEvent productEvent)throws CimbleException {
		if(cimbleAdminProductEventDao.save(productEvent) != 0)
		{
			return productEvent;
		}
		return new ProductEvent();
	}
		
	@Override
	public List<ProductEvent> listProductsEvents(ProductEventFilter filter,
			Paging paging) throws CimbleException {
				return cimbleAdminProductEventDao.list(filter, paging);
			}

	@Override
	public int deleteProductEvent(ProductEventFilter filter)
			throws CimbleException {
		return cimbleAdminProductEventDao.delete(filter);
	}

	@Override
	public ProductEvent updateProductEvent(ProductEvent productEvent,
			ProductEventFilter filter)throws CimbleException {
				if(cimbleAdminProductEventDao.update(productEvent,filter) != 0)
				{
					return productEvent;
				}
				return new ProductEvent();
			}
	

	@Override
	public ProductEvent findProductEventById(String id) throws CimbleException {
		return cimbleAdminProductEventDao.findById(id);
	}
	

	@Override
	public ProductEvent findOneProductEvent(ProductEventFilter filter) {
		return cimbleAdminProductEventDao.findOne(filter);
	}

}
