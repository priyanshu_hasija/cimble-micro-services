package com.cimble.admin.management;

import java.util.List;

import com.cimble.admin.dto.Partner;
import com.cimble.admin.filter.PartnerFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public interface CimbleAdminManagement {

	/**
	 * 
	 * @param partner
	 * @return
	 * @throws CimbleException
	 */
	Partner save(Partner partner) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<Partner> list(PartnerFilter filter, Paging paging)
			throws CimbleException;

	int delete(PartnerFilter filter) throws CimbleException;
	
	Partner update(Partner partner, PartnerFilter filter) throws CimbleException;
	
	Partner findById(String id) throws CimbleException;

	Partner findOne(PartnerFilter filter);

	String suspend(String id) throws CimbleException;

	String remove(String id) throws CimbleException;

	String assignToken(String token, String id) throws CimbleException;

}
