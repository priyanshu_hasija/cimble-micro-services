package com.cimble.admin.management;

import java.util.List;

import com.cimble.admin.dto.Product;
import com.cimble.admin.filter.ProductFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */
public interface CimbleAdminProductManagement {

	/**
	 * 
	 * @param partner
	 * @return
	 * @throws CimbleException
	 */
	Product saveProduct(Product product) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<Product> listProducts(ProductFilter filter, Paging paging)
			throws CimbleException;

	int deleteProduct(ProductFilter filter) throws CimbleException;
	
	Product updateProduct(Product product, ProductFilter filter) throws CimbleException;
	
	Product findProductById(String id) throws CimbleException;

	Product findOneProduct(ProductFilter filter);

	String clone(Integer curr_id, Integer new_id) throws CimbleException;

	List<Product> productsByPartner(ProductFilter filter, Paging convertValue); 
	

}
