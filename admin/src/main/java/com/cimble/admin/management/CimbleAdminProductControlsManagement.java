package com.cimble.admin.management;
import java.util.List;

import com.cimble.admin.dto.ProductControls;
import com.cimble.admin.filter.ProductControlsFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;

/**
 *This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public interface CimbleAdminProductControlsManagement {
	
	/**
	 * 
	 * @param product Controls
	 * @return
	 * @throws CimbleException
	 */
	ProductControls saveProductControls(ProductControls productControls) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<ProductControls> listProductControls(ProductControlsFilter filter, Paging paging)
			throws CimbleException;
	
int deleteProductControls(ProductControlsFilter filter) throws CimbleException;
	
	ProductControls updateProductControls(ProductControls productControls, ProductControlsFilter filter) throws CimbleException;
	
	ProductControls findProductControlsById(String id) throws CimbleException;

	ProductControls findProductControlsOne(ProductControlsFilter filter);

}
