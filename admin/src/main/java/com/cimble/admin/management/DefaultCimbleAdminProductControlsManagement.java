package com.cimble.admin.management;

import java.util.List;

import com.cimble.admin.dao.CimbleAdminProductControlsDAO;
import com.cimble.admin.dto.ProductControls;
import com.cimble.admin.filter.ProductControlsFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */

public class DefaultCimbleAdminProductControlsManagement implements CimbleAdminProductControlsManagement {
	private CimbleAdminProductControlsDAO cimbleAdminProductControlsDao;

	public DefaultCimbleAdminProductControlsManagement(CimbleAdminProductControlsDAO cimbleAdminProductControlsDao) {
		super();
		this.cimbleAdminProductControlsDao = cimbleAdminProductControlsDao;
	}

	@Override
	public List<ProductControls> listProductControls(ProductControlsFilter filter, Paging paging)
			throws CimbleException {
		return cimbleAdminProductControlsDao.list(filter, paging);
	}

	@Override
	public ProductControls saveProductControls(ProductControls productControls) throws CimbleException {
		if(cimbleAdminProductControlsDao.save (productControls) != 0)
		{
			return productControls;
		}
		return new ProductControls();
	}
	
	@Override
	public int deleteProductControls(ProductControlsFilter filter) throws CimbleException {
		return cimbleAdminProductControlsDao.delete(filter);
	}
	
	@Override
	public ProductControls updateProductControls(ProductControls productControls,ProductControlsFilter filter) throws CimbleException {
		if(cimbleAdminProductControlsDao.update(productControls,filter) != 0)
		{
			return productControls;
		}
		return new ProductControls();
	}
	
	@Override
	public ProductControls findProductControlsById(String id) throws CimbleException {
		return cimbleAdminProductControlsDao.findById(id);
	}

	@Override
	public ProductControls findProductControlsOne(ProductControlsFilter filter) {
		return cimbleAdminProductControlsDao.findOne(filter);
	}


}
