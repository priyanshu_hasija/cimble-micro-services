package com.cimble.admin.management;

import java.util.List;

import com.cimble.admin.dao.CimbleAdminDAO;
import com.cimble.admin.dto.Partner;
import com.cimble.admin.filter.PartnerFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public class DefaultCimbleAdminManagement implements CimbleAdminManagement {
	private CimbleAdminDAO cimbleAdminDAO;

	public DefaultCimbleAdminManagement(CimbleAdminDAO cimbleAdminDAO) {
		super();
		this.cimbleAdminDAO = cimbleAdminDAO;
	}

	@Override
	public List<Partner> list(PartnerFilter filter, Paging paging)
			throws CimbleException {
		return cimbleAdminDAO.list(filter, paging);
	}

	@Override
	public Partner save(Partner partner) throws CimbleException {
		if(cimbleAdminDAO.save (partner) != 0)
		{
			return partner;
		}
		return new Partner();
	}
	
	@Override
	public int delete(PartnerFilter filter) throws CimbleException {
		return cimbleAdminDAO.delete(filter);
	}
	
	@Override
	public Partner update(Partner partner,PartnerFilter filter) throws CimbleException {
		if(cimbleAdminDAO.update(partner,filter) != 0)
		{
			return partner;
		}
		return new Partner();
	}
	
	@Override
	public Partner findById(String id) throws CimbleException {
		return cimbleAdminDAO.findById(id);
	}

	@Override
	public Partner findOne(PartnerFilter filter) {
		return cimbleAdminDAO.findOne(filter);
	}

	@Override
	public String suspend(String id) throws CimbleException {
		if(cimbleAdminDAO.suspend(id) != 0)
			return "Partner Suspended";
		else
			return "Partner Not Suspended";
	}

	@Override
	public String remove(String id) throws CimbleException {
		if(cimbleAdminDAO.remove(id) != 0)
			return "Partner Removed Successfully";
		else
			return "Partner Not Removed";
	}

	@Override
	public String assignToken(String token, String id) throws CimbleException {
		if(cimbleAdminDAO.assignToken(token,id) != 0)
			return "Token Assigned Successfully";
		else
			return "Token Not Assigned";
	}

}
