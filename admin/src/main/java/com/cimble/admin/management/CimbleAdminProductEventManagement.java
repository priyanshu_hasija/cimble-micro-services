package com.cimble.admin.management;

import java.util.List;

import com.cimble.admin.dto.ProductEvent;
import com.cimble.admin.filter.ProductEventFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */
public interface CimbleAdminProductEventManagement {

	/**
	 * 
	 * @param partner
	 * @return
	 * @throws CimbleException
	 */
	ProductEvent saveProductEvent(ProductEvent productEvent) throws CimbleException;

	/**
	 * 
	 * @param filter
	 * @param paging
	 * @return
	 * @throws CimbleException
	 */
	List<ProductEvent> listProductsEvents(ProductEventFilter filter, Paging paging)
			throws CimbleException;

	int deleteProductEvent(ProductEventFilter filter) throws CimbleException;
	
	ProductEvent updateProductEvent(ProductEvent productEvent, ProductEventFilter filter) throws CimbleException;
	
	ProductEvent findProductEventById(String id) throws CimbleException;

	ProductEvent findOneProductEvent(ProductEventFilter filter);

}
