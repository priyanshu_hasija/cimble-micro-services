	package com.cimble.admin.management;

import java.util.List;

import com.cimble.admin.dao.CimbleAdminProductDAO;
import com.cimble.admin.dto.Product;
import com.cimble.admin.filter.ProductFilter;
import com.cimble.exception.CimbleException;
import com.cimble.paging.Paging;
import com.cimble.sort.SortRules;

/**
 * This interface is intended for providing interactions between dao classes
 * related to manage.
 * 
 * @version 1.0
 */
public class DefaultCimbleAdminProductManagement implements CimbleAdminProductManagement{
	
	private CimbleAdminProductDAO cimbleAdminProductDao;

	public DefaultCimbleAdminProductManagement(CimbleAdminProductDAO cimbleAdminProductDAO) {
		super();
		this.cimbleAdminProductDao = cimbleAdminProductDAO;
	}

	@Override
	public List<Product> listProducts(ProductFilter filter, Paging paging)
			throws CimbleException {
		String orderQuery = "group_id asc";
		SortRules sortRules = new SortRules();
		sortRules.parse(orderQuery);
		sortRules.setRuleQuery(sortRules.getRules());
		System.out.println("#######management: "+sortRules.ruleQuery);
		return cimbleAdminProductDao.orderedList(filter, paging,sortRules );
	}

	@Override
	public Product saveProduct(Product product) throws CimbleException {
		if(cimbleAdminProductDao.save (product) != 0)
		{
			return product;
		}
		return new Product();
	}
	
	@Override
	public int deleteProduct(ProductFilter filter) throws CimbleException {
		return cimbleAdminProductDao.delete(filter);
	}
	
	@Override
	public Product updateProduct(Product product,ProductFilter filter) throws CimbleException {
		if(cimbleAdminProductDao.update(product,filter) != 0)
		{
			return product;
		}
		return new Product();
	}
	
	@Override
	public Product findProductById(String id) throws CimbleException {
		return cimbleAdminProductDao.findById(id);
	}

	@Override
	public Product findOneProduct(ProductFilter filter) {
		return cimbleAdminProductDao.findOne(filter);
	}

	@Override
	public String clone(Integer curr_id, Integer new_id) throws CimbleException {
		cimbleAdminProductDao.clone(curr_id, new_id);
			return "Product cloned successfully";
	}

	@Override
	public List<Product> productsByPartner(ProductFilter filter, Paging convertValue) {
		// TODO Auto-generated method stub
		return null;
	}

}
