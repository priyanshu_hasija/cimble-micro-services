package com.cimble.admin.bootstrap;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.skife.jdbi.v2.DBI;

import com.cimble.admin.config.CimbleAdminBootstrapConfiguration;
import com.cimble.admin.dao.CimbleAdminDAO;
import com.cimble.admin.dao.CimbleAdminProductControlsDAO;
import com.cimble.admin.dao.CimbleAdminProductDAO;
import com.cimble.admin.dao.CimbleAdminProductEventDAO;
import com.cimble.admin.management.CimbleAdminManagement;
import com.cimble.admin.management.CimbleAdminProductControlsManagement;
import com.cimble.admin.management.CimbleAdminProductEventManagement;
import com.cimble.admin.management.CimbleAdminProductManagement;
import com.cimble.admin.management.DefaultCimbleAdminManagement;
import com.cimble.admin.management.DefaultCimbleAdminProductControlsManagement;
import com.cimble.admin.management.DefaultCimbleAdminProductEventManagement;
import com.cimble.admin.management.DefaultCimbleAdminProductManagement;
import com.cimble.admin.sevice.impl.DefaultCimbleAdminProductControlsService;
import com.cimble.admin.sevice.impl.DefaultCimbleAdminProductEventService;
import com.cimble.admin.sevice.impl.DefaultCimbleAdminProductService;
import com.cimble.admin.sevice.impl.DefaultCimbleAdminService;
import com.cimble.bootstrap.AbstractBootstrap;

/**
 * This class is intended for starting the application. It registers service, ui
 * & db migration modules.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public class CimbleAdminBootstrap extends
		AbstractBootstrap<CimbleAdminBootstrapConfiguration> {

	public static final String ID = "cimble";
	private CimbleAdminManagement cimbleAdminManagement;
	private CimbleAdminProductManagement cimbleAdminProductManagement;
	private CimbleAdminProductControlsManagement cimbleAdminProductControlsManagement;
	private CimbleAdminProductEventManagement cimbleAdminProductEventManagement;

	public static void main(String[] args) throws Exception {
		new CimbleAdminBootstrap().run(args);
	}

	@Override
	public void initializeConfig(
			Bootstrap<CimbleAdminBootstrapConfiguration> bootstrap) {
		bootstrap.addBundle(new MigrationsBundle<CimbleAdminBootstrapConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(CimbleAdminBootstrapConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
	}

	@Override
	public void runBootstrap(CimbleAdminBootstrapConfiguration configuration,
			Environment environment) throws Exception {
		DBI dbi = new DBI(configuration.getDataSourceFactory().getUrl(),
				configuration.getDataSourceFactory().getUser(), configuration
						.getDataSourceFactory().getPassword());

		CimbleAdminDAO ingestDao = dbi.onDemand(CimbleAdminDAO.class);

		cimbleAdminManagement = new DefaultCimbleAdminManagement(ingestDao);
		

		environment.jersey().register(
				new DefaultCimbleAdminService(cimbleAdminManagement));
		
		CimbleAdminProductDAO ingestProductDao = dbi.onDemand(CimbleAdminProductDAO.class);

		cimbleAdminProductManagement = new DefaultCimbleAdminProductManagement(ingestProductDao);
			

		environment.jersey().register(
				new DefaultCimbleAdminProductService(cimbleAdminProductManagement));
		
		CimbleAdminProductControlsDAO ingestProductControlsDao = dbi.onDemand(CimbleAdminProductControlsDAO.class);

		cimbleAdminProductControlsManagement = new DefaultCimbleAdminProductControlsManagement(ingestProductControlsDao);

		environment.jersey().register(
				new DefaultCimbleAdminProductControlsService(cimbleAdminProductControlsManagement));
	
	CimbleAdminProductEventDAO ingestProductEventDao = dbi.onDemand(CimbleAdminProductEventDAO.class);

	cimbleAdminProductEventManagement = new DefaultCimbleAdminProductEventManagement(ingestProductEventDao);

	environment.jersey().register(
			new DefaultCimbleAdminProductEventService(cimbleAdminProductEventManagement));
}

	@Override
	public String getId() {
		return ID;
	}

}
