package com.cimble.admin.filter;

import com.cimble.filter.SearchFilter;

/**
 * This class is responsible for holding generic fields to be returned in
 * response.
 * .
 * @version 1.0
 */
public class ProductControlsFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private Integer productId;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	
	
	
	
}