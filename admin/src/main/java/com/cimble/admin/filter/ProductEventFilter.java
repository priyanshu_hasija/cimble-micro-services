package com.cimble.admin.filter;

import java.sql.ResultSet;

import com.cimble.admin.dto.Partner;
import com.cimble.filter.SearchFilter;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Partner}
 * .
 * @version 1.0
 */
public class ProductEventFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private Integer productId;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	
	
	
	
	
	
}
