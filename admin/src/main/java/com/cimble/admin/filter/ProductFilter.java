package com.cimble.admin.filter;

import com.cimble.filter.SearchFilter;

/**
 * This class is responsible for holding generic fields to be returned in
 * response.
 * .
 * @version 1.0
 */
public class ProductFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private Integer partnerId;
	private String name;
	
	
	public Integer getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
