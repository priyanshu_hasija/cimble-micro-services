package com.cimble.admin.filter;

import com.cimble.filter.SearchFilter;

/**
 * This class is responsible for holding generic fields to be returned in
 * response.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public class PartnerFilter extends SearchFilter {
	private static final long serialVersionUID = -9155632280790356699L;
	
	private String name;
	

	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
