package com.cimble.admin.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.admin.dto.Product;
import com.cimble.admin.filter.ProductFilter;
import com.cimble.mapper.RequestMapper;
/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Product_Controls}
 * .
 * 
 * @version 1.0
 */

public class ProductMapper implements ResultSetMapper<Product>,
   RequestMapper<Product, ProductFilter>{
	
	private static final String TABLE_NAME = "PRODUCT";
	
	
	private static final String ADMIN_EMAIL = "ADMIN_EMAIL";
	private static final String CARRIER_ID ="CARRIER_ID";
	private static final String CC_BILLING_NAME = "CC_BILLING_NAME";
	private static final String DEFAULT_HOME = "DEFAULT_HOME";
	private static final String E911_P ="E911_P";
	private static final String DESCRIPTION = "DESCRIPTION";
	private static final String FROM_ADDRESS ="FROM_ADDRESS";
	private static final String PARTNER_ID ="PARTNER_ID";
	private static final String IVR ="IVR";
	private static final String JSP_TEMPLATE_ROOT = "JSP_TEMPLATE_ROOT";
	private static final String MALLSITE_P ="MALLSITE_P";
	private static final String MAPPING_SERVICE ="MAPPING_SERVICE";
	private static final String MIN_TYPE ="MIN_TYPE";
	private static final String NAME ="NAME";
	private static final String OBJECT_ID = "OBJECT_ID";
	private static final String PACKET_TYPE ="PACKET_TYPE";
	private static final String ID ="ID";
	private static final String REQUIRES_INSTALLER_LOG = "REQUIRES_INSTALLER_LOG";
	private static final String SUBDIR ="SUBDIR";
	private static final String SUPPORTS_NOTIFICATION_EDIT_P ="SUPPORTS_NOTIFICATION_EDIT_P";
	private static final String SUSPEND_TYPE = "SUSPEND_TYPE";
	private static final String TEMPLATE_P ="TEMPLATE_P";
	private static final String TEMPLATE_ROOT ="TEMPLATE_ROOT";
	private static final String TIME_STAMP = "TIME_STAMP";


	@Override
	public Product map(int index, ResultSet rs, StatementContext ctx)
			throws SQLException {
		// TODO Auto-generated method stub
		Product obj = new Product();

		obj.setAdminEmail(rs.getString(ADMIN_EMAIL));
		obj.setCarrierId(rs.getLong(CARRIER_ID));
		obj.setCcBillingName(rs.getString(CC_BILLING_NAME));
		obj.setDefaultHome(rs.getString(DEFAULT_HOME));
		obj.setAdminIp(rs.getString(E911_P));
		obj.setDesc(rs.getString(DESCRIPTION));
		obj.setFromAddress(rs.getString(FROM_ADDRESS));
		obj.setPartnerId(rs.getInt(PARTNER_ID));
		obj.setIvr(rs.getString(IVR));
		obj.setJspTemplateRoot(rs.getString(JSP_TEMPLATE_ROOT));
		obj.setMallSitep(rs.getInt(MALLSITE_P));
		obj.setMappingService(rs.getInt(MAPPING_SERVICE));
		obj.setName(rs.getString(NAME));
		obj.setMintype(rs.getString(MIN_TYPE));
		obj.setObjectId(rs.getInt(OBJECT_ID));
		obj.setPacketType(rs.getString(PACKET_TYPE));
		obj.setId(rs.getString(ID));
		obj.setRequiresInstallerLog(rs.getLong(REQUIRES_INSTALLER_LOG));
		obj.setTemplateRoot(rs.getString(TEMPLATE_ROOT));
		obj.setSuspendType(rs.getString(SUSPEND_TYPE));
		obj.setSupportsNotificationEditP(rs.getLong(SUPPORTS_NOTIFICATION_EDIT_P));
		obj.setSubdir(rs.getString(SUBDIR));
		obj.setTemplateP(rs.getLong(TEMPLATE_P));
		obj.setTimeStamp(rs.getLong(TIME_STAMP));
		
		return obj;
	}

@Override
public Map<String, Object> convertToDao(Product dto) {
	Map<String, Object> map = new HashMap<String, Object>();
	if (dto == null) {
		return map;
	}
	if (dto.getPartnerId() != null)
		map.put(PARTNER_ID,dto.getPartnerId());
	if (StringUtils.isNotBlank(dto.getAdminEmail()))
		map.put(ADMIN_EMAIL, dto.getAdminEmail());
	if ((dto.getCarrierId()) != null)
		map.put(CARRIER_ID, dto.getCarrierId());
	if (StringUtils.isNotBlank(dto.getJspTemplateRoot()))
		map.put(JSP_TEMPLATE_ROOT, dto.getJspTemplateRoot());
	if (StringUtils.isNotBlank(dto.getCcBillingName()))
		map.put(CC_BILLING_NAME, dto.getCcBillingName());
	if (StringUtils.isNotBlank(dto.getAdminIp()))
		map.put(E911_P, dto.getAdminIp());
	if (StringUtils.isNotBlank(dto.getDesc()))
		map.put(DESCRIPTION, dto.getDesc());
	if (StringUtils.isNotBlank(dto.getDefaultHome()))
		map.put(DEFAULT_HOME, dto.getDefaultHome());
	if (StringUtils.isNotBlank(dto.getFromAddress()))
		map.put(FROM_ADDRESS, dto.getFromAddress());
	if (StringUtils.isNotBlank(dto.getIvr()))
		map.put(IVR, dto.getIvr());
	if (StringUtils.isNotBlank(dto.getMintype()))
		map.put(MIN_TYPE, dto.getMintype());
	if (StringUtils.isNotBlank(dto.getPacketType()))
		map.put(PACKET_TYPE, dto.getPacketType());
	if (StringUtils.isNotBlank(dto.getTemplateRoot()))
		map.put(TEMPLATE_ROOT, dto.getTemplateRoot());
	if (StringUtils.isNotBlank(dto.getSuspendType()))
		map.put(SUSPEND_TYPE, dto.getSuspendType());
	if (StringUtils.isNotBlank(dto.getSubdir()))
		map.put(SUBDIR, dto.getSubdir());
	if (dto.getMallSitep() != null)
		map.put(MALLSITE_P, dto.getMallSitep());
	if (dto.getMappingService() != null)
		map.put(MAPPING_SERVICE, dto.getMappingService());
	if (dto.getObjectId() != null)
		map.put(OBJECT_ID, dto.getObjectId());
	if (StringUtils.isNotBlank(dto.getId()))
		map.put(ID, dto.getId());
	if (StringUtils.isNotBlank(dto.getName()))
		map.put(NAME, dto.getName());
	if (dto.getRequiresInstallerLog() != null)
		map.put(REQUIRES_INSTALLER_LOG, dto.getRequiresInstallerLog());
	if (dto.getSupportsNotificationEditP() != null)
		map.put(SUPPORTS_NOTIFICATION_EDIT_P, dto.getSupportsNotificationEditP());
	if (dto.getTemplateP() != null)
		map.put(TEMPLATE_P, dto.getTemplateP());
	if ((dto.getTimeStamp()) != null)
		map.put(TIME_STAMP, dto.getTimeStamp());
	if (dto.getCreatedOn() != null)
		map.put(Product.CREATED_ON, dto.getCreatedOn());
	if (dto.getModifiedOn() != null)
		map.put(Product.MODIFIED_ON, dto.getModifiedOn());
	
	return map;
}

@Override
public Map<String, Object> convertToDao(ProductFilter filter) {
	Map<String, Object> map = new HashMap<String, Object>();
	if (filter == null) {
		return map;
	}
	else {
		// set filter values
		if(filter.getPartnerId() != null)
			map.put(PARTNER_ID, filter.getPartnerId());
		if(StringUtils.isNotBlank(filter.getName()))
			map.put(NAME, filter.getName());
		if(StringUtils.isNotBlank(filter.getId()))
			map.put(ID, filter.getId());

		return map;
	}
}

@Override
public String getTableName() {
	return TABLE_NAME;
}


}