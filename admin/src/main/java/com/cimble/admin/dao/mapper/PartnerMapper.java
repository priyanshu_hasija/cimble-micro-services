package com.cimble.admin.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.admin.dto.Partner;
import com.cimble.admin.filter.PartnerFilter;
import com.cimble.mapper.RequestMapper;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Partner}
 * .
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public class PartnerMapper implements ResultSetMapper<Partner>,
		RequestMapper<Partner, PartnerFilter> {

	private static final String TABLE_NAME = "PARTNER";
	
	private static final String ID = "ID";
	private static final String URL ="URL";
	private static final String GROUP_TEMPLATE = "GROUP_TEMPLATE";
	private static final String JSP_TEMPLATE_ROOT = "JSP_TEMPLATE_ROOT";
	private static final String ADMIN_EMAIL = "ADMIN_EMAIL";
	private static final String ADMIN_IP ="ADMIN_IP";
	private static final String PRETTY_NAME ="PRETTY_NAME";
	private static final String FORCE_SSL ="FORCE_SSL";
	private static final String EXTRA_PERMISSION_P ="EXTRA_PERMISSION_P";
	private static final String TOKEN = "TOKEN";
	private static final String NAME ="NAME";
	private static final String IS_ACTIVE ="IS_ACTIVE";
	private static final String TIME_STAMP = "TIME_STAMP";

	@Override
	public Partner map(int index, ResultSet rs, StatementContext sc)
			throws SQLException {
		Partner obj = new Partner();

		obj.setId(rs.getString(ID));
		obj.setUrl(rs.getString(URL));
		obj.setGroupTemplate(rs.getString(GROUP_TEMPLATE));
		obj.setJspTemplateRoot(rs.getString(JSP_TEMPLATE_ROOT));
		obj.setAdminEmail(rs.getString(ADMIN_EMAIL));
		obj.setAdminIp(rs.getString(ADMIN_IP));
		obj.setPrettyName(rs.getString(PRETTY_NAME));
		obj.setForceSsl(rs.getLong(FORCE_SSL));
		obj.setExtraPermisssionP(rs.getLong(EXTRA_PERMISSION_P));
		obj.setToken(rs.getString(TOKEN));
		obj.setName(rs.getString(NAME));
		obj.setIsActive(rs.getInt(IS_ACTIVE));
		obj.setTimeStamp(rs.getLong(TIME_STAMP));
		
		return obj;
	}

	@Override
	public Map<String, Object> convertToDao(Partner dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (StringUtils.isNotBlank(dto.getId()))
			map.put(ID, dto.getId());
		if (StringUtils.isNotBlank(dto.getUrl()))
			map.put(URL, dto.getUrl());
		if (StringUtils.isNotBlank(dto.getGroupTemplate()))
			map.put(GROUP_TEMPLATE, dto.getGroupTemplate());
		if (StringUtils.isNotBlank(dto.getJspTemplateRoot()))
			map.put(JSP_TEMPLATE_ROOT, dto.getJspTemplateRoot());
		if (StringUtils.isNotBlank(dto.getAdminEmail()))
			map.put(ADMIN_EMAIL, dto.getAdminEmail());
		if (StringUtils.isNotBlank(dto.getAdminIp()))
			map.put(ADMIN_IP, dto.getAdminIp());
		if (StringUtils.isNotBlank(dto.getPrettyName()))
			map.put(PRETTY_NAME, dto.getPrettyName());
		if (dto.getForceSsl() != null)
			map.put(FORCE_SSL, dto.getForceSsl());
		if (dto.getExtraPermisssionP() != null)
			map.put(EXTRA_PERMISSION_P, dto.getExtraPermisssionP());
		if (StringUtils.isNotBlank(dto.getToken()))
			map.put(TOKEN, dto.getToken());
		if (StringUtils.isNotBlank(dto.getName()))
			map.put(NAME, dto.getName());
		if (dto.getIsActive() != null)
			map.put(IS_ACTIVE, dto.getIsActive());
		if(dto.getTimeStamp()!= null)
			map.put(TIME_STAMP, dto.getTimeStamp());
		if (dto.getCreatedOn() != null)
			map.put(Partner.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(Partner.MODIFIED_ON, dto.getModifiedOn());
		
		return map;
	}

	@Override
	public Map<String, Object> convertToDao(PartnerFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
			if(StringUtils.isNotBlank(filter.getId()))
				map.put(ID, filter.getId());
			if(StringUtils.isNotBlank(filter.getName()))
				map.put(NAME, filter.getName());

			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

}