package com.cimble.admin.dao;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.admin.dao.mapper.ProductEventMapper;
import com.cimble.admin.dto.ProductEvent;
import com.cimble.admin.filter.ProductEventFilter;
import com.cimble.dao.AbstractRelationalDao;



	@RegisterMapper(ProductEventMapper.class)
	@UseStringTemplate3StatementLocator
	public abstract class CimbleAdminProductEventDAO extends
			AbstractRelationalDao<ProductEvent, ProductEventFilter, ProductEventMapper> {

		public CimbleAdminProductEventDAO() throws Exception {
			super(ProductEventMapper.class);
		}
	}





