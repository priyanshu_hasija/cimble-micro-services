package com.cimble.admin.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.admin.dto.Product;
import com.cimble.admin.dto.ProductEvent;
import com.cimble.admin.filter.ProductEventFilter;
import com.cimble.mapper.RequestMapper;
/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Product Event}
 * .
 * 
 * @version 1.0
 */

public class ProductEventMapper implements ResultSetMapper<ProductEvent>,
   RequestMapper<ProductEvent, ProductEventFilter>{
	private static final String TABLE_NAME = "PRODUCT_EVENTS";
	private static final String ACTIVE= "ACTIVE";
	private static final String DEFAULT_P = "DEFAULT_P";
	private static final String DEVICE_EVENT_ID ="DEVICE_EVENT_ID";
	private static final String EVENT_CODE = "EVENT_CODE";
	private static final String ID = "ID";
	private static final String EVENT_ORDER ="EVENT_ORDER";
	private static final String INPUT = "INPUT";
	private static final String PARENT_EVENT ="PARENT_EVENT";
	private static final String PRODUCT_ID ="PRODUCT_ID";
	private static final String REDIRECT_P ="REDIRECT_P";
	private static final String VISIBLE = "VISIBLE";
	private static final String TIME_STAMP = "TIME_STAMP";	
	private static final String DESCRIPTION ="DESCRIPTION";
	private static final String EVENT_TYPE ="EVENT_TYPE";
	@Override
	public ProductEvent map(int index, ResultSet rs, StatementContext ctx)
			throws SQLException {
		// TODO Auto-generated method stub
		ProductEvent obj = new ProductEvent();

		obj.setActive(rs.getInt(ACTIVE));
		obj.setDefaultP(rs.getInt(DEFAULT_P));
		obj.setDeviceEventId(rs.getInt(DEVICE_EVENT_ID));
		obj.setEventCode(rs.getInt(EVENT_CODE));
		obj.setId(rs.getString(ID));
		obj.setEventOrder(rs.getInt(EVENT_ORDER));
		obj.setInput(rs.getInt(INPUT));
		obj.setParentEvent(rs.getInt(PARENT_EVENT));
		obj.setProductId(rs.getInt(PRODUCT_ID));
		obj.setRedirectP(rs.getInt(REDIRECT_P));
		obj.setVisible(rs.getInt(VISIBLE));
		obj.setTimeStamp(rs.getLong(TIME_STAMP));
		obj.setEventType(rs.getString(EVENT_TYPE));
		obj.setDescription(rs.getString(DESCRIPTION));
		return obj;
	}

@Override
public Map<String, Object> convertToDao(ProductEvent dto) {
	Map<String, Object> map = new HashMap<String, Object>();
	if (dto == null) {
		return map;
	}
	if (dto.getActive() != null)
		map.put(ACTIVE, dto.getActive());
	if (dto.getDefaultP() != null)
		map.put(DEFAULT_P, dto.getDefaultP());
	if (dto.getDeviceEventId() != null)
		map.put(DEVICE_EVENT_ID, dto.getDeviceEventId());
	if (dto.getEventCode() != null)
		map.put(EVENT_CODE, dto.getEventCode());
	if (StringUtils.isNotBlank(dto.getId()))
		map.put(ID, dto.getId());
	if (dto.getEventOrder() != null)
		map.put(EVENT_ORDER, dto.getEventOrder());
	if (dto.getInput() != null)
		map.put(INPUT, dto.getInput());
	if (dto.getParentEvent() != null)
		map.put(PARENT_EVENT, dto.getParentEvent());
	if (dto.getProductId() != null)
		map.put(PRODUCT_ID, dto.getProductId());
	if (dto.getRedirectP() != null)
		map.put(REDIRECT_P, dto.getRedirectP());
	if (dto.getVisible() != null)
		map.put(VISIBLE, dto.getVisible());
	if ((dto.getTimeStamp()) != null)
		map.put(TIME_STAMP, dto.getTimeStamp());
	if (dto.getCreatedOn() != null)
		map.put(ProductEvent.CREATED_ON, dto.getCreatedOn());
	if (dto.getModifiedOn() != null)
		map.put(ProductEvent.MODIFIED_ON, dto.getModifiedOn());
	if(StringUtils.isNotBlank(dto.getEventType()))
		map.put(EVENT_TYPE, dto.getEventType());
	if(StringUtils.isNotBlank(dto.getDescription()))
		map.put(DESCRIPTION, dto.getDescription());
	return map;
}

@Override
public Map<String, Object> convertToDao(ProductEventFilter filter) {
	Map<String, Object> map = new HashMap<String, Object>();
	if (filter == null) {
		return map;
	}
	else {
		// set filter values
		if(StringUtils.isNotBlank(filter.getId()))
			map.put(ID, filter.getId());
		
		if(filter.getProductId() != null)
			map.put(PRODUCT_ID, filter.getProductId());

		return map;
	}
}

@Override
public String getTableName() {
	return TABLE_NAME;
}


}
