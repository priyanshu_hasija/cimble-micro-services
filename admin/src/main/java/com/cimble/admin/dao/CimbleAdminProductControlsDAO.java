package com.cimble.admin.dao;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.admin.dao.mapper.ProductControlsMapper;
import com.cimble.admin.dto.ProductControls;
import com.cimble.admin.filter.ProductControlsFilter;
import com.cimble.dao.AbstractRelationalDao;

/**
 * This interface is intended for providing interactions with Product_controls table.
 * 
 * @version 1.0
 */

	@RegisterMapper(ProductControlsMapper.class)
	@UseStringTemplate3StatementLocator
	public abstract class CimbleAdminProductControlsDAO extends
			AbstractRelationalDao<ProductControls, ProductControlsFilter, ProductControlsMapper> {

		public CimbleAdminProductControlsDAO() throws Exception {
			super(ProductControlsMapper.class);
		}
	}
