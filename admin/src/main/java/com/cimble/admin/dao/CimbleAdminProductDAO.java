package com.cimble.admin.dao;

import java.sql.CallableStatement;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.admin.dao.mapper.ProductMapper;
import com.cimble.admin.dto.Product;
import com.cimble.admin.filter.ProductFilter;
import com.cimble.dao.AbstractRelationalDao;



/**
 * This interface is intended for providing interactions with product table.
 * 
 * @version 1.0
 */
@RegisterMapper(ProductMapper.class)
@UseStringTemplate3StatementLocator
public abstract class CimbleAdminProductDAO extends
AbstractRelationalDao<Product, ProductFilter, ProductMapper> {

	public CimbleAdminProductDAO() throws Exception {
		super(ProductMapper.class);
	}

	public void clone(Integer curr_id, Integer new_id) {

		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			java.sql.Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@cimbledb.cpj2vl1dmx2k.us-west-1.rds.amazonaws.com:1521:ORCL","AD2","letmein");
			String query = "BEGIN CLONE_PRODUCT(?, ?); END;";
			CallableStatement stmt = conn.prepareCall(query);// REF CURSOR
			//set the in parammeter
			stmt.setInt(1,curr_id);
			stmt.setInt(2,new_id);
			stmt.execute();
		}
		catch (SQLException e) {
			System.out.println("Product not cloned");
			e.printStackTrace();
		}

	}
}





