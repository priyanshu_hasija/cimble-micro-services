package com.cimble.admin.dao.mapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.cimble.admin.dto.ProductControls;
import com.cimble.admin.filter.ProductControlsFilter;
import com.cimble.mapper.RequestMapper;

/**
 * This class is intended for mapping a row of {@link ResultSet} to {@link Product_Controls}
 * .
 * @version 1.0
 */
public class ProductControlsMapper implements ResultSetMapper<ProductControls>,
RequestMapper<ProductControls, ProductControlsFilter>{
	
	
	private static final String TABLE_NAME = "PRODUCT_CONTROLS";
	
	private static final String ACK_P = "ACK_P";
	private static final String ACTIVE ="ACTIVE";
	private static final String CLONE_CONTROL_ID = "CLONE_CONTROL_ID";
	private static final String CONTROL = "CONTROL";
	private static final String ID ="ID";
	private static final String CONTROL_TYPE = "CONTROL_TYPE";
	private static final String DEFAULT_P ="DEFAULT_P";
	private static final String DESCRIPTION ="DESCRIPTION";
	private static final String DISPLAY_ORDER ="DISPLAY_ORDER";
	private static final String GOVERNOR_STATE = "GOVERNOR_STATE";
	private static final String IMG ="IMG";
	private static final String OFFSET ="OFFSET";
	private static final String PARENT_CONTROL ="PARENT_CONTROL";
	private static final String PRODUCT_ID ="PRODUCT_ID";
	private static final String REQUIRES_FOCC_ACK_P = "REQUIRES_FOCC_ACK_P";
	private static final String RETRIES ="RETRIES";
	private static final String RETRY_DELAY ="RETRY_DELAY";
	private static final String SYSTEM_EVENT = "SYSTEM_EVENT";
	private static final String VISIBLE ="VISIBLE";
	private static final String TIME_STAMP = "TIME_STAMP";	
	private static final String ICON_PATH = "ICON_PATH";
	@Override
	public ProductControls map(int index, ResultSet rs, StatementContext ctx)
			throws SQLException {
		// TODO Auto-generated method stub
		ProductControls obj = new ProductControls();

		obj.setAckP(rs.getInt(ACK_P));
		obj.setActive(rs.getInt(ACTIVE));
		obj.setCloneControlId(rs.getLong(CLONE_CONTROL_ID));
		obj.setControl(rs.getString(CONTROL));
		obj.setId(rs.getString(ID));
		obj.setControlType(rs.getString(CONTROL_TYPE));
		obj.setDefaultP(rs.getInt(DEFAULT_P));
		obj.setDescription(rs.getString(DESCRIPTION));
		obj.setDisplayOrder(rs.getInt(DISPLAY_ORDER));
		obj.setGovernorState(rs.getString(GOVERNOR_STATE));
		obj.setImg(rs.getString(IMG));
		obj.setOffset(rs.getString(OFFSET));
		obj.setParentControl(rs.getInt(PARENT_CONTROL));
		obj.setRequiresFoccAckP(rs.getLong(REQUIRES_FOCC_ACK_P));
		obj.setRetries(rs.getInt(RETRIES));
		obj.setRetryDelay(rs.getInt(RETRY_DELAY));
		obj.setProductId(rs.getLong(PRODUCT_ID));
		obj.setSystemEvent(rs.getString(SYSTEM_EVENT));
		obj.setVisible(rs.getInt(VISIBLE));
		obj.setTimeStamp(rs.getLong(TIME_STAMP));
		obj.setIconPath(rs.getString(ICON_PATH));

		return obj;
	}

	@Override
	public Map<String, Object> convertToDao(ProductControls dto) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (dto == null) {
			return map;
		}
		if (dto.getProductId() != null)
			map.put(PRODUCT_ID, dto.getProductId());
		if (StringUtils.isNotBlank(dto.getControl()))
			map.put(CONTROL, dto.getControl());
		if (StringUtils.isNotBlank(dto.getControlType()))
			map.put(CONTROL_TYPE, dto.getControlType());
		if (StringUtils.isNotBlank(dto.getDescription()))
			map.put(DESCRIPTION, dto.getDescription());
		if (StringUtils.isNotBlank(dto.getGovernorState()))
			map.put(GOVERNOR_STATE, dto.getGovernorState());
		if (StringUtils.isNotBlank(dto.getImg()))
			map.put(IMG, dto.getImg());
		if (StringUtils.isNotBlank(dto.getOffset()))
			map.put(OFFSET, dto.getOffset());
		if (StringUtils.isNotBlank(dto.getSystemEvent()))
			map.put(SYSTEM_EVENT, dto.getSystemEvent());
		if (dto.getAckP() != null)
			map.put(ACK_P, dto.getAckP());
		if (dto.getActive() != null)
			map.put(ACTIVE, dto.getActive());
		if (dto.getCloneControlId() != null)
			map.put(CLONE_CONTROL_ID, dto.getCloneControlId());
		if (dto.getDefaultP() != null)
			map.put(DEFAULT_P, dto.getDefaultP());
		if (StringUtils.isNotBlank(dto.getId()))
			map.put(ID, dto.getId());
		if (dto.getDisplayOrder() != null)
			map.put(DISPLAY_ORDER, dto.getDisplayOrder());
		if (dto.getParentControl() != null)
			map.put(PARENT_CONTROL, dto.getParentControl());
		if (dto.getRequiresFoccAckP() != null)
			map.put(REQUIRES_FOCC_ACK_P, dto.getRequiresFoccAckP());
		if (dto.getRetryDelay() != null)
			map.put(RETRY_DELAY, dto.getRetryDelay());
		if (dto.getRetries() != null)
			map.put(RETRIES, dto.getRetries());
		if (dto.getVisible() != null)
			map.put(VISIBLE, dto.getVisible());
		if ((dto.getTimeStamp()) != null)
			map.put(TIME_STAMP, dto.getTimeStamp());
		if (dto.getCreatedOn() != null)
			map.put(ProductControls.CREATED_ON, dto.getCreatedOn());
		if (dto.getModifiedOn() != null)
			map.put(ProductControls	.MODIFIED_ON, dto.getModifiedOn());
		if (StringUtils.isNotBlank(dto.getIconPath()))
			map.put(ICON_PATH, dto.getIconPath());
		return map;
	}

	public Map<String, Object> convertToDao(ProductControlsFilter filter) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (filter == null) {
			return map;
		}
		else {
			// set filter values
		
			if(StringUtils.isNotBlank(filter.getId()))
				map.put(ID, filter.getId());
			if(filter.getProductId() != null)
				map.put(PRODUCT_ID, filter.getProductId());

			return map;
		}
	}

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}


}
