package com.cimble.admin.dao;

import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.admin.dao.mapper.PartnerMapper;
import com.cimble.admin.dto.Partner;
import com.cimble.admin.filter.PartnerFilter;
import com.cimble.dao.AbstractRelationalDao;
import com.cimble.exception.CimbleException;

/**
 * This interface is intended for providing interactions with Partner table.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
@RegisterMapper(PartnerMapper.class)
@UseStringTemplate3StatementLocator
public abstract class CimbleAdminDAO extends
		AbstractRelationalDao<Partner, PartnerFilter, PartnerMapper> {

	public CimbleAdminDAO() throws Exception {
		super(PartnerMapper.class);
	}

	public int suspend(String id) throws CimbleException {
		return update("PARTNER", "is_Active = 0",WHERE + "group_id = "+Integer.parseInt(id));
	}

	public int remove(String id) throws CimbleException {
		return update("PARTNER", "is_Active = 0, token = ''",WHERE + "group_id = "+Integer.parseInt(id));
	}

	public int assignToken(String token, String id) throws CimbleException{
		return update("PARTNER", "is_Active = 1, token = '"+token+"'",WHERE + "group_id = "+Integer.parseInt(id));
	}

}
