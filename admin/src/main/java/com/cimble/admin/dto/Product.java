package com.cimble.admin.dto;

import com.cimble.dto.AbstractAuditDTO;

/**
 * This class is intended for holding product information.
 * 
 * @version 1.0
 */

public class Product extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L;

	private Integer partnerId;
	private Long carrierId;
	private String ccBillingName;
	private String defaultHome;
	private String adminIp;
	private String desc;
	private String fromAddress;
	private String ivr;
	private String jspTemplateRoot;
	private Integer mallSitep;
	private Integer mappingService;
	private String mintype;
	private Integer objectId;
	private String packetType;
	private Long requiresInstallerLog;
	private String subdir;
	private Long supportsNotificationEditP;
	private String suspendType;
	private String name;
	private Long templateP;
	private String templateRoot ;
	private String adminEmail ;
	private Long timeStamp;
	
	

	
	public Integer getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}
	public Long getCarrierId() {
		return carrierId;
	}
	public void setCarrierId(Long carrierId) {
		this.carrierId = carrierId;
	}
	public Long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getCcBillingName() {
		return ccBillingName;
	}
	public void setCcBillingName(String ccBillingName) {
		this.ccBillingName = ccBillingName;
	}
	public String getDefaultHome() {
		return defaultHome;
	}
	public void setDefaultHome(String defaultHome) {
		this.defaultHome = defaultHome;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	public String getIvr() {
		return ivr;
	}
	public void setIvr(String ivr) {
		this.ivr = ivr;
	}
	public String getJspTemplateRoot() {
		return jspTemplateRoot;
	}
	public void setJspTemplateRoot(String jspTemplateRoot) {
		this.jspTemplateRoot = jspTemplateRoot;
	}
	public Integer getMallSitep() {
		return mallSitep;
	}
	public void setMallSitep(Integer mallSitep) {
		this.mallSitep = mallSitep;
	}
	public Integer getMappingService() {
		return mappingService;
	}
	public void setMappingService(Integer mappingService) {
		this.mappingService = mappingService;
	}
	public String getMintype() {
		return mintype;
	}
	public void setMintype(String mintype) {
		this.mintype = mintype;
	}
	public Integer getObjectId() {
		return objectId;
	}
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
	public String getPacketType() {
		return packetType;
	}
	public void setPacketType(String packetType) {
		this.packetType = packetType;
	}
	
	
	public String getSubdir() {
		return subdir;
	}
	public void setSubdir(String subdir) {
		this.subdir = subdir;
	}
	
	public Long getRequiresInstallerLog() {
		return requiresInstallerLog;
	}
	public void setRequiresInstallerLog(Long requiresInstallerLog) {
		this.requiresInstallerLog = requiresInstallerLog;
	}
	public Long getSupportsNotificationEditP() {
		return supportsNotificationEditP;
	}
	public void setSupportsNotificationEditP(Long supportsNotificationEditP) {
		this.supportsNotificationEditP = supportsNotificationEditP;
	}
	public String getSuspendType() {
		return suspendType;
	}
	public void setSuspendType(String suspendType) {
		this.suspendType = suspendType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getTemplateP() {
		return templateP;
	}
	
	public String getTemplateRoot() {
		return templateRoot;
	}
	public void setTemplateP(Long templateP) {
		this.templateP = templateP;
	}
	public void setTemplateRoot(String templateRoot) {
		this.templateRoot = templateRoot;
	}
	public String getAdminEmail() {
		return adminEmail;
	}
	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getAdminIp() {
		return adminIp;
	}
	public void setAdminIp(String adminIp) {
		this.adminIp = adminIp;
	}

}