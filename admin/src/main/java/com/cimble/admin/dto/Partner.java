package com.cimble.admin.dto;

import com.cimble.dto.AbstractAuditDTO;

/**
 * This class is intended for holding ingest information.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public class Partner extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L;

	private String url;
	private String groupTemplate;
	private String jspTemplateRoot;
	private String adminEmail;
	private String adminIp;
	private String prettyName;
	private Long forceSsl;
	private Long extraPermisssionP;
	private String token;
	private String name;
	private Integer isActive;
	private Long timeStamp;

	

	public Long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getGroupTemplate() {
		return groupTemplate;
	}

	public void setGroupTemplate(String groupTemplate) {
		this.groupTemplate = groupTemplate;
	}

	public String getJspTemplateRoot() {
		return jspTemplateRoot;
	}

	public void setJspTemplateRoot(String jspTemplateRoot) {
		this.jspTemplateRoot = jspTemplateRoot;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getAdminIp() {
		return adminIp;
	}

	public void setAdminIp(String adminIp) {
		this.adminIp = adminIp;
	}

	public String getPrettyName() {
		return prettyName;
	}

	public void setPrettyName(String prettyName) {
		this.prettyName = prettyName;
	}

	public Long getForceSsl() {
		return forceSsl;
	}

	public void setForceSsl(Long forceSsl) {
		this.forceSsl = forceSsl;
	}

	public Long getExtraPermisssionP() {
		return extraPermisssionP;
	}

	public void setExtraPermisssionP(Long extraPermisssionP) {
		this.extraPermisssionP = extraPermisssionP;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

}
