package com.cimble.admin.dto;



import com.cimble.dto.AbstractAuditDTO;

/**
 * This class is intended for holding ingest information.
 * 
 * @version 1.0
 */

public class ProductEvent extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L;

	private Integer active;
	private Integer defaultP;
	private Integer deviceEventId;
	private Integer eventCode;
	private Integer eventOrder;
	private Integer input;
	private Integer parentEvent;
	private Integer productId;
	private Integer redirectP;
	private Integer visible;
	private Long timeStamp;
	private String description;
	private String eventType;
	
	public Long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Integer getDefaultP() {
		return defaultP;
	}
	public void setDefaultP(Integer defaultP) {
		this.defaultP = defaultP;
	}
	public Integer getDeviceEventId() {
		return deviceEventId;
	}
	public void setDeviceEventId(Integer deviceEventId) {
		this.deviceEventId = deviceEventId;
	}
	public Integer getEventCode() {
		return eventCode;
	}
	public void setEventCode(Integer eventCode) {
		this.eventCode = eventCode;
	}
	
	public Integer getEventOrder() {
		return eventOrder;
	}
	public void setEventOrder(Integer eventOrder) {
		this.eventOrder = eventOrder;
	}
	public Integer getInput() {
		return input;
	}
	public void setInput(Integer input) {
		this.input = input;
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getRedirectP() {
		return redirectP;
	}
	public void setRedirectP(Integer redirectP) {
		this.redirectP = redirectP;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}
	public Integer getParentEvent() {
		return parentEvent;
	}
	public void setParentEvent(Integer parentEvent) {
		this.parentEvent = parentEvent;
	}
	public Integer getVisible() {
		return visible;
	}
	public void setVisible(Integer visible) {
		this.visible = visible;
	}
	
	
	
	

	


}