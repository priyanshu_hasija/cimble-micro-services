package com.cimble.admin.dto;

import com.cimble.dto.AbstractAuditDTO;
/**
 * This interface is intended for providing interactions with product_controls table.
 * 
 * @version 1.0
 */

public class ProductControls  extends AbstractAuditDTO {
	private static final long serialVersionUID = 1303591606690521238L; 
	
	private Integer ackP;
	private Integer active;
	private Long cloneControlId;
	private String control;
	private String controlType;
	private Integer defaultP;
	private String description;
	private Integer displayOrder;
	private String governorState;
	private Integer parentControl;
	private String img;
	private Long productId;
	private String offset;
	private Long requiresFoccAckP;
	private Integer retryDelay;
	private Integer retries;
	private Integer visible;
	private String systemEvent;
	private Long timeStamp;
	private String iconPath;
	
	public Integer getAckP() {
		return ackP;
	}
	public void setAckP(Integer ackP) {
		this.ackP = ackP;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}
	public Long getCloneControlId() {
		return cloneControlId;
	}
	public void setCloneControlId(Long cloneControlId) {
		this.cloneControlId = cloneControlId;
	}
	public String getControl() {
		return control;
	}
	public void setControl(String control) {
		this.control = control;
	}
	public String getControlType() {
		return controlType;
	}
	public void setControlType(String controlType) {
		this.controlType = controlType;
	}
	public Integer getDefaultP() {
		return defaultP;
	}
	public void setDefaultP(Integer defaultP) {
		this.defaultP = defaultP;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	public String getGovernorState() {
		return governorState;
	}
	public void setGovernorState(String governorState) {
		this.governorState = governorState;
	}
	public Integer getParentControl() {
		return parentControl;
	}
	public void setParentControl(Integer parentControl) {
		this.parentControl = parentControl;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getOffset() {
		return offset;
	}
	public void setOffset(String offset) {
		this.offset = offset;
	}
	public Long getRequiresFoccAckP() {
		return requiresFoccAckP;
	}
	public void setRequiresFoccAckP(Long requiresFoccAckP) {
		this.requiresFoccAckP = requiresFoccAckP;
	}
	public Integer getRetryDelay() {
		return retryDelay;
	}
	public void setRetryDelay(Integer retryDelay) {
		this.retryDelay = retryDelay;
	}
	public Integer getRetries() {
		return retries;
	}
	public void setRetries(Integer retries) {
		this.retries = retries;
	}
	public Integer getVisible() {
		return visible;
	}
	public void setVisible(Integer visible) {
		this.visible = visible;
	}
	public String getSystemEvent() {
		return systemEvent;
	}
	public void setSystemEvent(String systemEvent) {
		this.systemEvent = systemEvent;
	}
	public Long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getIconPath() {
		return iconPath;
	}
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
