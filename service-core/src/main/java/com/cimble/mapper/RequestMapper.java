package com.cimble.mapper;

import java.util.Map;

import com.cimble.dto.AbstractDTO;
import com.cimble.filter.SearchFilter;

/**
 * This class is responsible for mapping of database request.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 *
 * @param <T>
 *            extends {@link AbstractDTO}
 * @param <E>
 *            extends {@link SearchFilter}
 */
public interface RequestMapper<T extends AbstractDTO, E extends SearchFilter> {

    Map<String, Object> convertToDao(T dto);

    Map<String, Object> convertToDao(E filter);

    String getTableName();

}
