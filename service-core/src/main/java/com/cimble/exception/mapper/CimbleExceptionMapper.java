package com.cimble.exception.mapper;

import java.util.Arrays;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cimble.exception.CimbleException;
import com.cimble.response.CommonResponse;
import com.cimble.utils.UUIDUtils;

/**
 * This class is intended for mapping exception to service response.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
@Provider
public class CimbleExceptionMapper implements ExceptionMapper<CimbleException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CimbleExceptionMapper.class.getName());

    @Override
    public Response toResponse(CimbleException e) {
        String errorId = UUIDUtils.getUUID();
        LOGGER.error("Exception in service for error id :" + errorId, e);
        return Response.ok(
                new CommonResponse().setErrorCode(new Integer(500).toString()).setErrors(Arrays.asList(e.getMessage()))
                        .addAdditionalData("errorId", errorId)).build();
    }

}
