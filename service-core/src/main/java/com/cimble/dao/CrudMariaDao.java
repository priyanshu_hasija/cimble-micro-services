package com.cimble.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.dto.AbstractDTO;
import com.cimble.paging.Paging;

/**
 * 
 * This class is intended for providing crud dao functions.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 *
 * @param <T>
 *            extends {@link AbstractDTO}
 */
@UseStringTemplate3StatementLocator
public interface CrudMariaDao<T extends AbstractDTO> {

    String TABLE = "table";
    String PARAMS = "params";
    String VALUES = "values";
    String QUERY = "query";
    String SET = "set";
    String AND = " and ";
    String WHERE = " where ";
    String RULEQUERY = "ruleQuery";

    /**
     * Save.
     * 
     * @param table
     * @param params
     * @param values
     * @return 1 for success & 0 for failure
     */
    @SqlUpdate("insert into <table>(<params>) values(<values>)")
    int save(@Define(TABLE) String table, @Define(PARAMS) String params, @Define(VALUES) String values);

    /**
     * Update
     * 
     * @param table
     * @param set
     * @param query
     * @return 1 for success & 0 for failure
     */
    @SqlUpdate("update <table> set <set> <query>")
    int update(@Define(TABLE) String table, @Define(SET) String set, @Define(QUERY) String query);

    /**
     * Find by id
     * 
     * @param id
     * @return find by id
     */
    @SqlQuery("select * from <table> where Id = :id")
    T findById(@Define(TABLE) String table, @Bind("id") String id);

    /**
     * List
     * 
     * @param table
     * @param query
     * @param paging
     * @return list of users for the provided account id
     */
    @SqlQuery("select * from <table> <query>")
    List<T> list(@Define(TABLE) String table, @Define(QUERY) String query, @BindBean Paging paging);

    /**
     * orderedList
     * 
     * @param table
     * @param query
     * @param orderbyfield
     * @param orderbyValue
     * @return ordered list of users for the provided account id
     */
    @SqlQuery("select * from <table> <query> ORDER BY <ruleQuery>")
    List<T> orderedList(@Define(TABLE) String table, @Define(QUERY) String query,@Define(RULEQUERY) String ruleQuery);
    
    /**
     * Find one
     * 
     * @param table
     * @param query
     * @return list of users for the provided account id
     */
    @SqlQuery("select * from <table> <query>")
    T findOne(@Define(TABLE) String table, @Define(QUERY) String query);

    /**
     * Delete
     * 
     * @param table
     * @param query
     * @return count of associations deleted
     */
    @SqlUpdate("delete from <table> <query>")
    int delete(@Define(TABLE) String table, @Define(QUERY) String query);

    @SqlUpdate("update <table> set status='0' where id= :id")
    int softDelete(@Define(TABLE) String table, @Bind("id") String id);

}
