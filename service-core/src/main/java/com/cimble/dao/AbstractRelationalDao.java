package com.cimble.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;

import org.apache.commons.lang.StringUtils;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import com.cimble.constants.Punctuation;
import com.cimble.dto.AbstractAuditDTO;
import com.cimble.dto.AbstractDTO;
import com.cimble.filter.SearchFilter;
import com.cimble.mapper.RequestMapper;
import com.cimble.paging.Paging;
import com.cimble.sort.SortRules;;

/**
 * 
 * This interface is intended for providing generic dao layer.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 * 
 * @param <T>
 *            extends {@link AbstractDTO}
 * @param <E>
 *            extends {@link SearchFilter}
 * @param <Z>
 *            extends {@link RequestMapper}
 */
@UseStringTemplate3StatementLocator
public abstract class AbstractRelationalDao<T extends AbstractDTO, E extends SearchFilter, Z extends RequestMapper<T, E>>
        implements CrudMariaDao<T> {

    private Z instance;

    public AbstractRelationalDao(Class<Z> requestMapper) throws Exception {
        super();
        instance = requestMapper.newInstance();
    }

    public int save(T dto) {
        AbstractAuditDTO b;
        if (dto instanceof AbstractAuditDTO) {
            b = (AbstractAuditDTO) dto;
            if (StringUtils.isEmpty(b.getId())) {
            	Integer new_id = java.util.concurrent.ThreadLocalRandom.current().nextInt(0, 50000 + 1);
                b.setId(new_id.toString());
            }
            if (b.getCreatedOn() == null) {
                b.setCreatedOn(new Date().getTime());
            }
            if (b.getModifiedOn() == null) {
                b.setModifiedOn(new Date().getTime());
            }
        }
        Map<String, Object> colMap = instance.convertToDao(dto);
        StringJoiner params = new StringJoiner(Punctuation.COMMA.toString());
        StringJoiner values = new StringJoiner(Punctuation.COMMA.toString());
        for (Entry<String, Object> entry : colMap.entrySet()) {
            params.add(entry.getKey());
            values.add(resolve(entry.getValue()));
        }
        return save(instance.getTableName(), params.toString(), values.toString());
    }

    public T findById(String ids) {
    	//System.out.println("@@@@@@@@@@@@"+ids);
        return findById(instance.getTableName(), ids);
    }

    public T findOne(E filter) {
        StringJoiner queryParams = join(AND, instance.convertToDao(filter));
        return findOne(instance.getTableName(), queryParams.length() == 0 ? Punctuation.SPACE.toString()
                : (WHERE + queryParams.toString()));
    }

    public List<T> list(E filter, Paging paging) {
        StringJoiner queryParams = join(AND, instance.convertToDao(filter));
        return list(instance.getTableName(), queryParams.length() == 0 ? Punctuation.SPACE.toString()
                : (WHERE + queryParams.toString()), paging);
    }
    
    public List<T> orderedList(E filter, Paging paging,SortRules sortRules) {
        StringJoiner queryParams = join(AND, instance.convertToDao(filter));
        System.out.println("#######relationalDAO"+sortRules.ruleQuery);
        return orderedList(instance.getTableName(), queryParams.length() == 0 ? Punctuation.SPACE.toString()
                : (WHERE + queryParams.toString()),sortRules.ruleQuery);
    }

    public int update(T dto, E filter) {
        if (dto.getId() != null && filter.getId() == null) {
            filter.setId(dto.getId());
        }
        if (dto instanceof AbstractAuditDTO) {
            if (((AbstractAuditDTO) dto).getModifiedOn() == null) {
                ((AbstractAuditDTO) dto).setModifiedOn(new Date().getTime());
            }
        }
        StringJoiner setFields = join(Punctuation.COMMA.toString(), instance.convertToDao(dto));
        if (setFields.length() == 0) {
            return 0;
        }
        StringJoiner queryParams = join(AND, instance.convertToDao(filter));
        return update(instance.getTableName(), setFields.toString(),
                queryParams.length() == 0 ? Punctuation.SPACE.toString() : (WHERE + queryParams.toString()));
    }

    private String resolve(Object val) {
        if (val instanceof String) {
            return Punctuation.SINGLE_QUOTE.toString()
                    + ((String) val).replace(Punctuation.SINGLE_QUOTE.toString(), Punctuation.SINGLE_QUOTE.toString()
                            + Punctuation.SINGLE_QUOTE.toString()) + Punctuation.SINGLE_QUOTE.toString();
        } else if (val instanceof Integer) {
            return String.valueOf(val);
        } else if (val instanceof Double) {
            return String.valueOf(val);
        } else if (val instanceof Long) {
            return String.valueOf(val);
        } else if (val instanceof Boolean) {
            return String.valueOf(val);
        } else if (val instanceof List) {
            // TODO Add csv
            System.out.println("Field Type Not Supported : " + val);
        } else {
            System.out.println("Field Type Not Supported : " + val);
        }
        return null;
    }

    public StringJoiner join(String joinWord, Map<String, Object> fieldMap) {
        StringJoiner joiner = new StringJoiner(joinWord);
        for (Entry<String, Object> entry : fieldMap.entrySet()) {
            String value = resolve(entry.getValue());
            if (value != null) {
                if (value.contains(Punctuation.COMMA.toString())) {
                    // TODO add logic
                } else {
                    joiner.add(entry.getKey() + "=" + value);
                }
            }
        }
        return joiner;
    }

    public int delete(E filter) {
    	StringJoiner queryParams = join(AND, instance.convertToDao(filter));
        return delete(instance.getTableName(), queryParams.length() == 0 ? Punctuation.SPACE.toString() : (WHERE + queryParams.toString()));
    }

    public int softDelete(E filter) {
    	StringJoiner queryParams = join(AND, instance.convertToDao(filter));
        return update(instance.getTableName(), "token = ''", queryParams.length() == 0 ? Punctuation.SPACE.toString() : (WHERE + queryParams.toString()));
    }
    
}
