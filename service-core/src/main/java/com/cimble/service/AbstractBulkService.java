package com.cimble.service;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.cimble.dto.AbstractDTO;
import com.cimble.exception.CimbleException;
import com.cimble.filter.SearchFilter;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * This class is intended for providing base services.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 * 
 * @param <T>
 *            extends {@link AbstractDTO}
 * @param <E>
 *            extends {@link SearchFilter}
 */
public abstract class AbstractBulkService<T extends AbstractDTO, E extends SearchFilter>
		extends AbstractService<T, E> {

	private Class<T> dtoType;
	private Class<E> filterType;
	
	public AbstractBulkService(Class<T> dtoType, Class<E> filterType) {
		super(dtoType, filterType);
	}

	/**
	 * Add new record
	 * 
	 * @param obj
	 * @return response
	 * @throws Exception
	 */
	protected abstract Response bulkSave(List<T> dtos) throws Exception;

	/**
	 * Update record
	 * 
	 * @param id
	 * @param dto
	 * @param filter
	 * @return response
	 * @throws Exception
	 */
//	protected abstract Response bulkUpdate(List<T> dtos, E filter)
//			throws Exception;
	
	protected abstract Response bulkUpdate(List<T> dtos)
			throws Exception;

	/**
	 * delete record
	 * 
	 * @param id
	 * @param dto
	 * @param filter
	 * @return response
	 * @throws Exception
	 */
	protected abstract Response bulkDelete(List<E> filter) throws Exception;

	@DELETE
	@Path("/bulk")
	public Response bulkDelete(String data)
			throws CimbleException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			List<E> list = (List<E>) mapper.convertValue(mapper.readValue(data, dtoType), filterType);
			return bulkDelete(list);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in delete()", e);
		}
	}

	/**
	 * Add new record
	 * 
	 * @param data
	 * @return response
	 * @throws CimbleException
	 */
	@POST
	@Path("/bulk")
	public Response bulkSave(String data) throws CimbleException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			List<T> list = mapper.readValue(data, mapper.getTypeFactory()
					.constructCollectionType(List.class, getDtoType()));
			return bulkSave(list);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in save()", e);
		}
	}

	/**
	 * Update record
	 * 
	 * @param id
	 * @param data
	 * @return response
	 * @throws CimbleException
	 */
	@PUT
	@Path("/bulk")
	public Response bulkUpdate(String data) throws CimbleException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			List<T> list = mapper.readValue(data, mapper.getTypeFactory()
					.constructCollectionType(List.class, getDtoType()));
			return bulkUpdate(list);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in update()", e);
		}
	}

}
