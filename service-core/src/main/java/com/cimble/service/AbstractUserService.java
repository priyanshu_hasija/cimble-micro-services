package com.cimble.service;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.cimble.dto.AbstractDTO;
import com.cimble.exception.CimbleException;
import com.cimble.filter.SearchFilter;
import com.cimble.paging.Paging;
import com.cimble.response.CommonResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractUserService<T extends AbstractDTO, E extends SearchFilter> {

	private Class<T> dtoType;
	private Class<E> filterType;
	
	public AbstractUserService(Class<T> dtoType, Class<E> filterType) {
		super();
		this.dtoType = dtoType;
		this.filterType = filterType;
	}
	
	/**
	 * Add new record
	 * 
	 * @param obj
	 * @return response
	 * @throws Exception
	 */
	protected abstract Response save(T dto,String id) throws Exception;

	/**
	 * Update record
	 * 
	 * @param id
	 * @param dto
	 * @param filter
	 * @param id 
	 * @return response
	 * @throws Exception
	 */
	protected abstract Response update(T dto, E filter, String id) throws Exception;

	/**
	 * List records
	 * 
	 * @param filter
	 * @param paging
	 * @return response
	 * @throws CimbleException
	 */
	protected abstract Response list(E filter,Paging paging,String id) throws Exception;

	/**
	 * Find by id
	 * 
	 * @param id
	 * @return response
	 * @throws CimbleException
	 */
	protected abstract Response findById(String id) throws Exception;

	/**
	 * Delete by id
	 * 
	 * @param id
	 * @return response
	 * @throws CimbleException
	 */
	protected abstract Response deleteById(String id) throws Exception;
	
	/**
	 * Delete by filter
	 * 
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	protected abstract Response delete(E filter) throws Exception;
	
	public Response onSuccess(Object data, Paging paging) {
		return Response
				.ok(new CommonResponse().setData(data).setPaging(paging)
						.toString()).build();
	}

	public Response onSuccess(Object data) {
		return Response.ok(new CommonResponse().setData(data).toString())
				.build();
	}

	public Response onError(int errorCode) {
		return Response.ok(
				new CommonResponse().setErrorCode(
						new Integer(errorCode).toString()).toString()).build();
	}

	public Response onError(int errorCode, List<String> errors) {
		return Response.ok(
				new CommonResponse().setErrorCode(
						new Integer(errorCode).toString()).toString()).build();
	}

	public Response onError(String errorCode) {
		return Response.ok(
				new CommonResponse().setErrorCode(errorCode).toString())
				.build();
	}

	public Response onError(String errorCode, List<String> errors) {
		return Response.ok(
				new CommonResponse().setErrorCode(errorCode).setErrors(errors)
						.toString()).build();
	}
	
	@POST
	public Response save(String data,@PathParam("id") String id) throws CimbleException
	{
		try {
			//System.out.println("saving partner");
			ObjectMapper mapper = new ObjectMapper();
			return save(mapper.readValue(data, dtoType),id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in save()", e);
		}
		
	}
	
	@PUT
	public Response update(String data,@PathParam("id") String id,@Context HttpServletRequest request) throws CimbleException
	{
		try {
			Map<String, String> map = new HashMap<>();
			Enumeration<String> params = request.getParameterNames();
			while (params.hasMoreElements()) {
				String param = params.nextElement();
				map.put(param, request.getParameter(param));
			}
			ObjectMapper mapper = new ObjectMapper();
			return update(mapper.readValue(data, dtoType),(E)mapper.convertValue(map,filterType),id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in update()", e);
		}
		
	}
	
	@GET
	public Response list(String data,@PathParam("id") String id,@Context HttpServletRequest request ) throws CimbleException
	{
		try {
			Map<String, String> map = new HashMap<>();
			Enumeration<String> params = request.getParameterNames();
			while (params.hasMoreElements()) {
				String param = params.nextElement();
				map.put(param, request.getParameter(param));
			}

			ObjectMapper mapper = new ObjectMapper();
			return list((E)mapper.convertValue(map,filterType),mapper.convertValue(map, Paging.class),id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CimbleException("Error in list()", e);
		}
		
	}
	
}
