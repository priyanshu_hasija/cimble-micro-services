package com.cimble.config;

import io.dropwizard.Configuration;

/**
 * This class is intended for holding all the configurations injected from the external *.yml file.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public class AbstractBootstrapConfiguration extends Configuration {

}
