package com.cimble.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;

/**
 * This class is responsible for holding config.
 * 
 * @author Priyanshu Hasija
 * @version 1.0
 */
public abstract class AbstractConfiguration {
    public static final String CONFIG_HOME = "CONFIG_HOME";
    private static final String DEFAULT_CONFIG_HOME = "/config";

    private static final Map<String, String> PROPS = new HashMap<String, String>();

    static {
        try {
            String home = System.getenv(CONFIG_HOME);
            if (StringUtils.isBlank(home)) {
                home = DEFAULT_CONFIG_HOME;
            }
            File dir = new File(home);
            if (dir != null && dir.exists()) {
                List<File> files = Arrays.asList(dir.listFiles());
                Map<Object, Object> configFiles = files
                        .stream()
                        .flatMap(file -> createPropertyMap(file))
                        .collect(Collectors.toMap(entry -> (String) entry.getKey(), entry -> (String) entry.getValue()));
                configFiles.forEach((k, v) -> PROPS.put((String) k, (String) v));
            } else {
                System.out.println("Missing config home directory. Please set CONFIG_HOME environment.");
                System.exit(0);
            }
        } catch (Exception e) {
            throw new RuntimeException("Error loading config", e);
        }
    }

    private static Stream<Entry<Object, Object>> createPropertyMap(File file) {
        try (FileInputStream fis = new FileInputStream(file)) {
            Properties properties = new Properties();
            properties.load(fis);
            return properties.entrySet().stream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This function is responsible for returning the value for given property key.
     * 
     * @param key
     * @return value
     */
    public static String getProperty(String key) {
        return PROPS.get(key);
    }

}
